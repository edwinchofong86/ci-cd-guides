# Bitbucket에서 GitLab으로 마이그레이션 가이드

본 가이드에서는 Bitbucket 리포지토리를 GitLab 프로젝트의 리포지토리로 마이그레이션하는 방법을 설명합니다.

GitLab의 [Bitbucket Server 가져오기 도구](https://docs.gitlab.com/ee/user/project/import/bitbucket_server.html)를 사용하여 Bitbucket 리포지토리의 다음 항목을 가져올 수 있습니다.

* 리포지토리 설명 (GitLab 11.2+)
* Git 리포지토리 데이터 (GitLab 11.2+)
* Pull requests (GitLab 11.2+)
* Pull request 코멘트 (GitLab 11.2+)

그러나, 사설망 또는 폐쇄망에 있는 Bitbucket Server는 GitLab이 액세스할 수 없어 가져오기 도구를 사용할 수 없습니다.

**Git clone의 `--mirror` 옵션을 사용하여 Bitbucket 리포지토리를 GitLab으로 마이그레이션할 수 있습니다.**  
이 방법은 Git 리포지토리 데이터(커밋 이력, 태그, 브랜치)만 마이그레이션 합니다.

## Bitbucket 리포지토리 GitLab으로 마이그레이션

### GitLab 프로젝트 생성

기존 Bitbucket 리포지토리의 데이터를 마이그레이션 할 대상인 신규 리포지토리를 구성하기 위해 GitLab 프로젝트를 생성합니다.

![GitLab new Project](images/08/gitlab_new_project.png "GitLab new Project")

* 상단 네비게이션 바에서 **[+]** 아이콘을 클릭하고 **New project/repository**를 선택하여 개인 프로젝트를 생성하거나, 그룹에서 **New project** 버튼을 클릭하여 프로젝트 생성합니다.
* **Create new project** 페이지에서 **Create blank project**를 클릭합니다.
* **Create blank project** 페이지에서 아래 항목을 입력 또는 선택하고 **Create project** 버튼을 클릭합니다.
  * **Project name** : 프로젝트 이름 입력 (예: `Migrating from Bitbucket to GitLab`)
  * **Visibility Level** : `Private` 선택
  * **Initialize repository with a README** : **반드시 체크 해제**

### 로컬 PC에서 Git 마이그레이션

* macOS에서는 터미널, Windows에서는 Git Bash 또는 명령 프롬프트(CMD)를 열고 작업 디렉토리로 이동합니다.

    ```bash
    cd ~/Workspace
    ```

* Bitbucket 리포지토리 URL를 복사하고 `--mirror` 옵션으로 Git Clone 합니다.

    ```bash
    git clone --mirror https://20xxxx5@mygit.skcc.com/scm/migtest/my-bitbucket-repo.git
    ```

    ![Bitbucket repository URL](images/08/bitbucket_repository_url.png "Bitbucket repository URL")

* `my-bitbucket-repo.git` 디렉토리로 이동합니다.

    ```bash
    cd my-bitbucket-repo.git
    ```

* GitLab 프로젝트에서 **Clone** 버튼을 클릭한 후, **Copy URL** 아이콘을 클릭하여 복사하고 원격 저장소 URL를 설정합니다.

    ```bash
    git remote set-url --push origin https://gitlab.com/skdt/devopsguide/examples_cicd/migrating-from-bitbucket-to-gitlab.git
    ```

    ![Copy GitLab repository URL](images/08/gitlab_copy_repository_url.png "Copy GitLab repository URL")

* 아래 명령을 실행하여 Local repository에 연결된 Remote repository를 확인합니다.

    ```bash
    $ git remote -v
    origin	https://20xxxx5@mygit.skcc.com/scm/migtest/my-bitbucket-repo.git (fetch)
    origin	https://gitlab.com/skdt/devopsguide/examples_cicd/migrating-from-bitbucket-to-gitlab.git (push)
    ```

* `--mirror` 옵션으로 신규 GitLab repository에 Push 합니다.

    ```bash
    git push --mirror
    ```

### 마이그레이션 결과 확인

아래와 같이 Bitbucket의 Git repository 데이터(커밋 이력 및 파일)를 GitLab 프로젝트의 Repository로 복사된 것을 확인할 수 있습니다.

![Bitbucket repository](images/08/bitbucket_repository.png "Bitbucket repository")

![GitLab repository](images/08/gitlab_repository.png "GitLab repository")

커밋 이력도 동일하게 복사된 것을 확인할 수 있습니다.

![Bitbucket commits](images/08/bitbucket_commits.png "Bitbucket commits")

![GitLab commits](images/08/gitlab_commits.png "GitLab commits")

모든 브랜치도 복사 됩니다.

![Bitbucket branches](images/08/bitbucket_branches.png "Bitbucket branches")

![GitLab branches](images/08/gitlab_branches.png "GitLab branches")

## 기존 Local repository의 Remote repository 변경

기존에 개발 업무를 수행하던 Local repository에 연결된 Remote repository를 신규 GitLab repository로 변경하면, 개발 환경을 수정하지 않아도 됩니다.

기존 Local repository로 이동한 후, 아래 명령을 실행합니다.

```bash
$ cd ~/Workspace/my-bitbucket-repo
$ git remote -v
origin	https://20xxxx5@mygit.skcc.com/scm/migtest/my-bitbucket-repo.git (fetch)
origin	https://20xxxx5@mygit.skcc.com/scm/migtest/my-bitbucket-repo.git (push)
```

아래 명령을 실행하여 Remote repository를 신규 GitLab repository로 설정합니다.

```bash
git remote set-url origin https://gitlab.com/skdt/devopsguide/examples_cicd/migrating-from-bitbucket-to-gitlab.git
```

다시 `git remote -v` 명령을 실행하면 Local repository에 연결된 Remote repository가 변경된 것을 확인할 수 있습니다.

```bash
git remote -v
origin	https://gitlab.com/skdt/devopsguide/examples_cicd/migrating-from-bitbucket-to-gitlab.git (fetch)
origin	https://gitlab.com/skdt/devopsguide/examples_cicd/migrating-from-bitbucket-to-gitlab.git (push)
```

## GitLab의 기본 브랜치 이름 변경

GitLab의 기본 브랜치 이름이 GitLab.com은 13.11 버전(2021년 4월 22일), 자체 관리형 GitLab은 14.0 버전(2021년 6월 22일)에 `master`에서 `main`으로 변경되었습니다.

자세한 내용은 [새로운 Git 기본 브랜치 이름](https://about.gitlab.com/blog/2021/03/10/new-git-default-branch-name/) 문서를 참고하세요.

### 로컬 `master`를 `main`으로 변경

아래 명령을 실행하여 로컬 `master` 브랜치 이름을 `main`으로 변경합니다.

```bash
git branch -m master main
```

`git status` 명령으로 브랜치 이름이 변경된 것을 확인할 수 있습니다.

```bash
$  git status
현재 브랜치 main
브랜치가 'origin/master'에 맞게 업데이트된 상태입니다.

커밋할 사항 없음, 작업 폴더 깨끗함
```

또한, `git branch` 명령을 실행하면 로컬 브랜치 목록을 확인할 수 있습니다.

원격 저장소에 새 `main` 브랜치로 Push 합니다.

```bash
$ git push -u origin main
Total 0 (delta 0), reused 0 (delta 0), pack-reused 0
remote:
remote: To create a merge request for main, visit:
remote:   https://gitlab.com/skdt/devopsguide/examples_cicd/migrating-from-bitbucket-to-gitlab/-/merge_requests/new?merge_request%5Bsource_branch%5D=main
remote:
To https://gitlab.com/skdt/devopsguide/examples_cicd/migrating-from-bitbucket-to-gitlab.git
 * [new branch]      main -> main
'main' 브랜치가 리모트의 'main' 브랜치를 ('origin'에서) 따라가도록 설정되었습니다.
```

이제 로컬 저장소가 원격 브랜치 이름으로 최신 상태임을 확인할 수 있습니다.

```bash
$ git status
현재 브랜치 main
브랜치가 'origin/main'에 맞게 업데이트된 상태입니다.

커밋할 사항 없음, 작업 폴더 깨끗함
```

### GitLab의 기본 브랜치 변경

이제 GitLab에 새 브랜치 `main`가 있으므로, 프로젝트 설정에서 기본 브랜치를 변경할 수 있습니다. 이 작업을 수행하려면 프로젝트에 대한 `Maintainer` 또는 `Owner` 액세스 권한이 있어야 합니다.

* 프로젝트의 **Settings > Repository**로 이동하여 **Default branch** 섹션을 확장합니다.
* **Default branch** 콤보박스에서 `main`을 선택하고 **Save changes** 버튼을 클릭합니다.

    ![GitLab Default branch](images/08/gitlab_default_branch.png "GitLab Default branch")

* **Protected branches** 섹션을 확장하고 아래 항목을 선택하고 **Protect** 버튼을 클릭합니다.
  * **Branch** : `main` 선택
  * **Allowed to merge** : `Maintainer` 선택
  * **Allowed to push** : `Maintainer` 선택
  * **Allowed to force push** : 비활성
  * **Require approval from code owners** : 비활성

    ![GitLab Protect branch](images/08/gitlab_protect_branch.png "GitLab Protect branch")

* Protected branches 목록에서 `master` 브랜치의 **Unprotect** 버튼을 클릭합니다.

    ![Protected branches](images/08/gitlab_protected_branches.png "Protected branches")

* 로컬 PC에서 아래 명령을 실행하여 GitLab의 이전 기본 브랜치였던 `master` 브랜치를 삭제합니다.

    ```bash
    $ git push origin --delete master
    To https://gitlab.com/skdt/devopsguide/examples_cicd/migrating-from-bitbucket-to-gitlab.git
    - [deleted]         master
    ```

* 프로젝트의 **Repository > Branches**로 이동하여 `master` 브랜치가 삭제된 것을 확인합니다.

    ![GitLab branches](images/08/gitlab_changed_branches.png "GitLab branches")
