# TestProject 연동 가이드

본 가이드에서는 Java용 TestProject OpenSDK을 사용하여 테스트 코드를 작성하고, TestProject platform(app.testproject.io) 및 TestProject agent와 연동하여 자동화 테스트를 수행하는 GitLab CI 파이프라인을 구성하는 방법을 설명합니다.

## 사전 조건

* GitLab CI 파이프라인을 구성하려는 프로젝트에 대한 `Maintainer` 또는 `Owner` 액세스 권한이 있어야 합니다.
* 로컬 PC에 Java 개발 환경이 구성되어 있어야 합니다. (JDK 11 이상, Maven, Git, IDE)

## TestProject 구성

### TestProject 계정 생성

TestProject 계정이 없으면 다음을 수행하여 계정을 생성합니다.

* [TestProject](https://testproject.io/)에서 이메일을 입력하고 **Create an Account** 버튼을 클릭하거나, Sign up with **Google** 또는 **Microsoft** 버튼을 클릭하여 계정을 생성합니다.

### TestProject Agent 설치

[TestProject Agent 다운로드](https://app.testproject.io/#/download) 페이지에서 운영체제(OS)에 해당하는 설치 파일을 다운로드 받은 후 설치하거나, [Docker Hub](https://hub.docker.com/r/testproject/agent)에서 컨테이너 이미지를 Pull하여 Docker agent를 설치합니다.

![Download TestProject Agent](images/05/download_testproject_agent.png "Download TestProject Agent")

### TestProject Agent 등록

다음을 수행하여 TestProject Agent 등록합니다.

* 상단 내비게이션 바에서 **Agents**를 클릭한 다음, **Register Agent**를 클릭합니다.
* **Agent Registration** 모달창에서 Agent 이름을 입력하고 **Save** 버튼을 클릭하면 Agent가 등록됩니다.

![Register TestProject Agent](images/05/register_testproject_agent.png "Register TestProject Agent")

### Developer token 설정

Developer token를 복사하여 `TP_DEV_TOKEN` 환경변수를 등록합니다.

* 상단 내비게이션 바에서 **Integrations**를 클릭하여 **SDK** 페이지로 이동합니다.
* **Copy to clipboard** 아이콘을 클릭하여 복사한 다음, 기록해 둡니다.
* (Mac/Linux) .bash_profile, .bashrc 또는 .zshrc에 `TP_DEV_TOKEN` 환경변수를 추가합니다.
* (Windows) **내 PC > 마우스 우클릭 > 속성 > 고급 시스템 설정 > 고급 탭 > 환경 변수**로 이동하여 `TP_DEV_TOKEN` 환경변수를 추가합니다.

![Developer token](images/05/developer_token.png "Developer token")

### API Key 생성

TestProject agent가 TestProject platform과 통신하기 위해 API Key가 필요합니다.

> API Key는 TestProject의 Admin만 생성할 수 있습니다.

* 상단 내비게이션 바에서 **Integrations**를 클릭한 다음, 사이드 바에서 **API**를 클릭합니다.
* **API Keys** 페이지에서 **Create API Key** 버튼을 클릭합니다.
* API Key 이름을 입력하고 **Next** 버튼을 클릭합니다.

  ![Create API Key #1](images/05/create_api_key_1.png "Create API Key #1")

* **Unrestricted**를 체크하고 **Finish** 버튼을 클릭합니다.

  ![Create API Key #2](images/05/create_api_key_2.png "Create API Key #2")

* **API Keys**가 생성되면 목록에 나타납니다.

  ![API Keys](images/05/api_keys.png "API Keys")

* **Copy** 클릭하여 복사한 다음, 기록해 둡니다.

### Docker Agent 설치

Docker Compose를 사용하여 GitLab CI에서 연동할 TestProject Agent를 설치합니다.

Linux 서버에 [Docker](https://docs.docker.com/engine/install/ubuntu/) 및 [Docker Compose](https://docs.docker.com/compose/install/)를 설치를 설치하고 다음을 수행합니다.

* 작업 디렉토리를 생성합니다.

  ```bash
  sudo mkdir testproject-agent
  cd testproject-agent
  sudo chown -R $USER:$USER /data/testproject-agent
  ```

* `docker-compose.yml`를 생성합니다. `<TP_API_KEY>`를 이전 단계에서 생성한 API Key로 변경합니다.

```bash
cat <<EOF > docker-compose.yml
version: "3.9"
services:
  testproject-agent:
    image: testproject/agent:latest
    container_name: testproject-agent
    depends_on:
      - chrome
      - firefox
    environment:
      TP_API_KEY: "<TP_API_KEY>"
      TP_AGENT_ALIAS: "Docker Agent"
      TP_AGENT_TEMP: "true"
      TP_SDK_PORT: "8686"
      CHROME: "chrome:4444"
      CHROME_EXT: "localhost:5555"
      FIREFOX: "firefox:4444"
      FIREFOX_EXT: "localhost:6666"
    ports:
      - "8585:8585"
      - "8686:8686"
  chrome:
    image: selenium/standalone-chrome
    volumes:
      - /dev/shm:/dev/shm
    ports:
      - "5555:4444"
  firefox:
    image: selenium/standalone-firefox
    volumes:
      - /dev/shm:/dev/shm
    ports:
      - "6666:4444"
EOF
```

* 아래 명령을 실행하면 TestProject Docker Agent가 실행되고 TestProject platform에 등록됩니다.

```bash
docker-compose up -d
```

* 설치한 Docker Agent를 TestProject Agents 페이지에서 확인할 수 있습니다.

![TestProject Agents](images/05/testproject_agents.png "TestProject Agents")

## Test 코드 작성

Java용 TestProject OpenSDK와 JUnit 5을 사용하여 테스트 코드를 작성합니다.

### `java-sdk` 의존성 라이브러리 추가

IDE에서 Maven 프로젝트를 구성하고 `pom.xml` 파일에 아래와 같이 의존성 라이브러리를 추가합니다.

```xml
<dependency>
  <groupId>io.testproject</groupId>
  <artifactId>java-sdk</artifactId>
  <version>1.1.0-RELEASE</version>
  <scope>test</scope>
  <exclusions>
      <exclusion>
          <groupId>org.testng</groupId>
          <artifactId>testng</artifactId>
      </exclusion>
  </exclusions>
</dependency>
```

> 현재 `java-sdk` dependency의 최신 버전은 `1.2.3-RELEASE`이나, 테스트 수행 시 Driver 연결이 안되는 오류가 있어 `1.1.0-RELEASE` 버전을 사용합니다.
> `testng`를 제외하여야 **Maven Surefire plugin**이 **JUnit 5**을 사용하는 테스트 메서드를 찾을 수 있습니다.

### Test 코드 예제

아래는 `ChromeDriver` 기반 로그인 테스트의 코드 예제입니다.

```java
import io.testproject.sdk.drivers.web.ChromeDriver;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeOptions;

import static org.junit.jupiter.api.Assertions.assertTrue;

@DisplayName("Testing Login")
public class LoginTest {

    /**
     * Driver instance
     */
    private static ChromeDriver driver;

    @BeforeAll
    static void setup() throws Exception {
        driver = new ChromeDriver(new ChromeOptions(), "My First Project", "My First Job");
    }

    @Test
    @DisplayName("Login Test Example")
    void loginTestExample() throws Exception {
        driver.navigate().to("https://example.testproject.io/web/");

        driver.findElement(By.cssSelector("#name")).sendKeys("Jason Lee");
        driver.findElement(By.cssSelector("#password")).sendKeys("12345");
        driver.findElement(By.cssSelector("#login")).click();

        boolean expectedResult = driver.findElement(By.cssSelector("#logout")).isDisplayed();

        assertTrue(expectedResult);
    }

    @AfterAll
    static void tearDown() {
        if (driver != null) {
            driver.quit();
        }
    }

}
```

프로젝트 및 작업 이름을 명시적으로 지정하여 Driver를 생성하면, 테스트 수행 결과가 지정한 프로젝트 및 작업 이름으로 Reports에 등록됩니다.

```java
ChromeDriver driver = new ChromeDriver(new ChromeOptions(), "My First Project", "My First Job");
```

Web Elements를 찾는 방법은 다음과 같이 몇 가지가 있습니다.

* ID : `id` 속성 값 (예: `By.id("password")`)
* Name : `name` 속성 값 (예: `By.name("username")`)
* ClassName : `class` 속성 값 (예: `By.className("button")`)
* CSS Selector : `#user_id`, `.button`와 같은 CSS 선택기 (예: `By.cssSelector("#name")`)
* XPath : XML 문서 쿼리하는 방식 (예: `By.xpath("//div[2]//input")`)
* Tag Name : 엘리먼트의 Tag 이름 (예: `By.tagName("button")`)
* Link Text : 하이퍼 링크의 텍스트 (예: `By.linkText("Click Me!")`)
* Partial Link Text : 하이퍼 링크 텍스트 문자열의 일부 (예: `By.partialLinkText("ck Me")`)

좀 더 상세한 내용은 [TestProject Documentation](https://docs.testproject.io/)와 [TestProject GitHub 리포지토리](https://github.com/testproject-io/java-opensdk)에서 예제와 문서를 확인합니다.

### Generated Code

**Test Recorder**로 생성한 프로젝트의 테스트 단계를 코드로 생성할 수 있습니다.

* TestProject의 **Projects** 페이지에서 코드를 생성할 프로젝트의 **···** 아이콘을 클릭하고 **Generated Code**를 선택합니다.

  ![Generated Code](images/05/generated_code.png "Generated Code")

* 원하는 프로그래밍 언어를 선택하고 **Download** 버튼을 클릭합니다.

  ![Export to code](images/05/export_to_code.png "Export to code")

* IDE에서 다운로드 받은 코드를 이용하여 테스트 프로젝트를 구성합니다.

### 로컬 PC에서 테스트

예제 프로젝트 [Test Automation With TestProject](https://gitlab.com/skdt/devopsguide/examples_cicd/test-automation-with-testproject)와 같이 코드베이스를 구성하고, 로컬 PC에서 테스트를 수행합니다.

> * `TP_DEV_TOKEN` 환경변수가 설정되어 있어야 합니다.
> * 기본적으로 Driver는 `http://localhost:8585`에서 수신하는 로컬 에이전트와 통신합니다. Remote Agent를 사용하력면 `TP_AGENT_URL` 환경변수를 추가해야 합니다.
> * Driver 생성하는 테스트 코드에서 지정한 프로젝트가 TestProject에 생성되어 있어야 합니다.

프로젝트 디렉토리에서 아래 명령을 실행하면 테스트가 진행됩니다.

```bash
mvn clean test
```

## GitLab CI 파이프라인 구성

### GitLab 프로젝트 생성 및 Git Push

코드베이스를 저장하고 CI 파이프라인을 구성하기 위한 GitLab 프로젝트를 생성합니다.

코드베이스를 GitLab Repository에 Push합니다.

* 터미널에서 테스트 프로젝트의 디렉토리로 이동합니다.(예 : `~/Workspace/test-automation-with-testproject`)
* `git init` 명령을 실행하여 로컬 Git 저장소를 생성하고 초기화합니다.

  ```bash
  cd ~/Workspace/test-automation-with-testproject
  git init
  ```

* 커밋할 대상 파일들을 추가하고 Git Commit 및 Push 합니다.

  ```bash
  git add .
  git commit -m "테스트 코드 추가"
  git remote add origin https://gitlab.com/skdt/xxxxx/test-automation-with-testproject.git
  git pull --allow-unrelated-histories origin main
  git push -u origin main
  ```

### GitLab CI/CD 환경 변수 생성

`.gitlab-ci.yml` 파일에서 호출할 환경 변수 `TP_DEV_TOKEN`와 `TP_AGENT_URL`를 생성합니다.

* 프로젝트의 **Settings > CI/CD**로 이동하여 **Variables** 섹션을 확장합니다.
* **Add Variable** 버튼을 클릭한 다음, **Add Variable** 모달 창에서 세부사항 입력하고 **Add variable** 버튼을 클릭합니다.
  * Key : `TP_DEV_TOKEN` 입력
  * Value : 이전 단계에서 생성하고 기록해 두었던 Developer token을 입력
  * Type : `Variable` 선택
  * Environment scope : `All` 선택
  * Protect variable (선택 사항) : 보호된 브랜치 또는 태그에서 실행되는 파이프라인에서만 변수를 사용하려면 체크

* 마찬가지 방법으로  `TP_AGENT_URL` 변수도 생성합니다.
  * Key : `TP_AGENT_URL` 입력
  * Value : 이전 단계에서 설치한 Docker Agent의 URL 입력 (예: `http://<Docker_Agent_IP>:8585`)

### `.gitlab-ci.yml` 파일 생성

* GitLab의 **Project overview** 페이지에서 project slug(`test-automation-with-testproject`) 오른쪽에 있는 **[+]** 아이콘을 클릭하고 **New file**를 선택합니다.
* **File name** 필드에 `.gitlab-ci.yml`를 입력합니다.
* 아래 내용을 복사하여 붙여넣습니다.

  ```yaml
  variables:
    MAVEN_OPTS: "-Dhttps.protocols=TLSv1.2 -Dmaven.repo.local=$CI_PROJECT_DIR/.m2/repository -Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=WARN -Dorg.slf4j.simpleLogger.showDateTime=true -Djava.awt.headless=true"
    MAVEN_CLI_OPTS: "--batch-mode --errors --fail-at-end --show-version -DinstallAtEnd=true -DdeployAtEnd=true"

  image: maven:3.8.1-openjdk-11

  test:
      before_script:
        - apt-get update
        - apt-get install -y jq
      script:
          - export TP_DEV_TOKEN=${TP_DEV_TOKEN}
          - export TP_AGENT_URL=${TP_AGENT_URL}
          - export IS_REGISTERED=$(curl -s $TP_AGENT_URL/api/status | jq '.registered')
          - echo "IS_REGISTERED = $IS_REGISTERED"
          - >
            if [ "$IS_REGISTERED" != true ]; then
              echo "There is no available TestProject agent."
              exit 1
            fi
          - mvn $MAVEN_CLI_OPTS clean test
      cache:
        paths:
          - .m2/repository
  ```

* **Commit message** 필드에 커밋 메시지를 입력합니다. (예: `Add initial .gitlab-ci.yml`)
* **Commit changes** 버튼을 클릭합니다.

### CI 파이프라인 결과 확인

* 사이드 바에서 **CI/CD > Pipelines**을 클릭하면 CI 파이프라인이 실행되는 것을 확인할 수 있습니다.
* 파이프라인 일련번호를 클릭하면 파이프라인 그래프를 볼 수 있습니다.
* `test` Job을 클릭하면 아래와 유사한 로그를 확인할 수 있습니다.

```bash
Running with gitlab-runner 13.12.0 (7a6612da)
  on skdt-group-runner-1 TNcP22Fe
Resolving secrets
00:00
Preparing the "docker" executor
Using Docker executor with image maven:3.8.1-openjdk-11 ...
Pulling docker image maven:3.8.1-openjdk-11 ...

(생략)

$ export TP_DEV_TOKEN=${TP_DEV_TOKEN}
$ export TP_AGENT_URL=${TP_AGENT_URL}
$ export IS_REGISTERED=$(curl -s $TP_AGENT_URL/api/status | jq '.registered')
$ echo "IS_REGISTERED = $IS_REGISTERED"
IS_REGISTERED = true
$ if [ "$IS_REGISTERED" != true ]; then # collapsed multi-line command
$ mvn $MAVEN_CLI_OPTS clean test
Apache Maven 3.8.1 (05c21c65bdfed0f71a2f2ada8b84da59348c4c5d)
Maven home: /usr/share/maven
Java version: 11.0.11, vendor: Oracle Corporation, runtime: /usr/local/openjdk-11
Default locale: en, platform encoding: UTF-8
OS name: "linux", version: "3.10.0-1127.el7.x86_64", arch: "amd64", family: "unix"
807 [INFO] Error stacktraces are turned on.
846 [INFO] Scanning for projects...
10764 [INFO] 
10765 [INFO] -----------------------< com.example:auto-test >------------------------
10766 [INFO] Building Test Automation 0.0.1-SNAPSHOT
10766 [INFO] --------------------------------[ jar ]---------------------------------
63275 [INFO] 
63276 [INFO] --- maven-clean-plugin:3.1.0:clean (default-clean) @ auto-test ---
70811 [INFO] 
70811 [INFO] --- maven-resources-plugin:3.2.0:resources (default-resources) @ auto-test ---
90187 [INFO] Using 'UTF-8' encoding to copy filtered resources.
90187 [INFO] Using 'UTF-8' encoding to copy filtered properties files.
90187 [INFO] skip non existing resourceDirectory /builds/skdt/devopsguide/examples_cicd/test-automation-with-testproject/src/main/resources
90187 [INFO] skip non existing resourceDirectory /builds/skdt/devopsguide/examples_cicd/test-automation-with-testproject/src/main/resources
90188 [INFO] 
90188 [INFO] --- maven-compiler-plugin:3.8.1:compile (default-compile) @ auto-test ---
105933 [INFO] No sources to compile
105933 [INFO] 
105933 [INFO] --- maven-resources-plugin:3.2.0:testResources (default-testResources) @ auto-test ---
105936 [INFO] Using 'UTF-8' encoding to copy filtered resources.
105936 [INFO] Using 'UTF-8' encoding to copy filtered properties files.
105936 [INFO] skip non existing resourceDirectory /builds/skdt/devopsguide/examples_cicd/test-automation-with-testproject/src/test/resources
105936 [INFO] 
105936 [INFO] --- maven-compiler-plugin:3.8.1:testCompile (default-testCompile) @ auto-test ---
105950 [INFO] Changes detected - recompiling the module!
105953 [INFO] Compiling 3 source files to /builds/skdt/devopsguide/examples_cicd/test-automation-with-testproject/target/test-classes
106755 [INFO] 
106755 [INFO] --- maven-surefire-plugin:2.22.2:test (default-test) @ auto-test ---
114137 [INFO] Surefire report directory: /builds/skdt/devopsguide/examples_cicd/test-automation-with-testproject/target/surefire-reports
116548 [INFO] 
116548 [INFO] -------------------------------------------------------
116548 [INFO]  T E S T S
116548 [INFO] -------------------------------------------------------
117059 [INFO] Running com.example.autotest.LoginTest
11:15:35.263 [main] INFO io.testproject.sdk.internal.rest.AgentClient - Initializing new session...
11:15:35.294 [main] DEBUG org.apache.http.client.protocol.RequestAddCookies - CookieSpec selected: default

(생략)

11:16:03.702 [Forwarding quit on session f5d1ff0dae507069fd5e42dc0e514420 to remote] INFO io.testproject.sdk.internal.rest.AgentClient - Session [f5d1ff0dae507069fd5e42dc0e514420] closed
145834 [INFO] Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 14.57 s - in com.example.autotest.ProjectListTest
11:16:03.721 [Thread-0] DEBUG io.testproject.sdk.internal.rest.AgentClient - Agent client is closing development socket as process is exiting...
11:16:03.721 [Thread-0] DEBUG io.testproject.sdk.internal.tcp.SocketManager - Disconnecting TCP development socket...
11:16:03.721 [Thread-0] DEBUG io.testproject.sdk.internal.tcp.SocketManager - Development socket closed
11:16:03.721 [Thread-0] INFO io.testproject.sdk.internal.rest.AgentClient - Session [f5d1ff0dae507069fd5e42dc0e514420] closed
146166 [INFO] 
146166 [INFO] Results:
146166 [INFO] 
146166 [INFO] Tests run: 4, Failures: 0, Errors: 0, Skipped: 0
146166 [INFO] 
146171 [INFO] ------------------------------------------------------------------------
146171 [INFO] BUILD SUCCESS
146171 [INFO] ------------------------------------------------------------------------
146173 [INFO] Total time:  02:25 min
146173 [INFO] Finished at: 2021-07-06T11:16:04Z
146173 [INFO] ------------------------------------------------------------------------
Saving cache for successful job
Creating cache default...
.m2/repository: found 2290 matching files and directories 
No URL provided, cache will be not uploaded to shared cache server. Cache will be stored only locally. 
Created cache
Cleaning up file based variables
00:00
Job succeeded
```

### UI 테스트 파이프라인 트리거

UI 테스트 대상 Web 애플리케이션를 배포하는 파이프라인에서 앞 단계의 UI 테스트 자동화 파이프라인을 트리거하면, 새로 배포된 애플리케이션의 UI 테스트를 수행할 수 있습니다.

[Trigger test automation pipeline](https://gitlab.com/skdt/devopsguide/examples_cicd/trigger-test-automation-pipeline) 프로젝트와 같이 애플리케이션을 배포하는 CD 파이프라인에서 [Test Automation With TestProject](https://gitlab.com/skdt/devopsguide/examples_cicd/test-automation-with-testproject) 프로젝트처럼 UI 테스트 자동화 파이프라인을 트리거 합니다.

아래는 CD 파이프라인에서 트리거하는 `.gitlab-ci.yml` 파일 예제 입니다.

```yaml
stages:
  - build
  - test
  - package
  - deploy
  - post-deploy

image: alpine:latest

build:
  stage: build
  script:
    - echo "애플리케이션을 빌드합니다."

test:
  stage: test
  script:
    - echo "애플리케이션의 단위 테스트를 수행합니다."

package:
  stage: package
  script:
    - echo "애플리케이션을 패키징합니다. (ex. build & push docker image)"

deploy:
  stage: deploy
  script:
    - echo "애플리케이션을 배포합니다."

auto-test:
  stage: post-deploy
  trigger: skdt/devopsguide/examples_cicd/test-automation-with-testproject
```

CD 파이프라인을 실행하면 Test Automation With TestProject의 파이프라인을 트리거하는 것을 파이프라인 그래프에서 확인할 수 있습니다.

![Triggering Pipeline Graph](images/05/pipeline_graph_trigger.png "Triggering Pipeline Graph")

### TestProject Reports

TestProject의 Reports 페이지에서 프로젝트를 선택하면 실행한 결과를 확인할 수 있습니다.

![TestProject Reports](images/05/testproject_reports.png "TestProject Reports")
