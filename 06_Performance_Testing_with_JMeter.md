# JMeter 연동 가이드

본 가이드에서는 Apache JMeter를 설치 및 구성하여 GUI에서 Test Plan을 작성한 후, JMeter CLI를 사용하여 Test Plan을 실행하고, HTML Dashboard Report를 생성하는 GitLab CI 파이프라인을 구성하는 방법을 설명합니다.

## 사전 조건

* GitLab CI 파이프라인을 구성하려는 프로젝트에 대한 `Maintainer` 또는 `Owner` 액세스 권한이 있어야 합니다.
* 로컬 PC에 JDK 8 이상이 설치되어 있어야 합니다.

## JMeter 설치

### macOS에 JMeter 설치

[Homebrew](https://brew.sh/)를 사용하여 JMeter을 설치합니다.

```bash
brew install jmeter
```

버전을 확인합니다.

```bash
$ jmeter -v
WARNING: package sun.awt.X11 not in java.desktop
    _    ____   _    ____ _   _ _____       _ __  __ _____ _____ _____ ____
   / \  |  _ \ / \  / ___| | | | ____|     | |  \/  | ____|_   _| ____|  _ \
  / _ \ | |_) / _ \| |   | |_| |  _|    _  | | |\/| |  _|   | | |  _| | |_) |
 / ___ \|  __/ ___ \ |___|  _  | |___  | |_| | |  | | |___  | | | |___|  _ <
/_/   \_\_| /_/   \_\____|_| |_|_____|  \___/|_|  |_|_____| |_| |_____|_| \_\ 5.4.1

Copyright (c) 1999-2021 The Apache Software Foundation
```

아래 명령을 실행하면 **JMeter GUI**를 실행할 수 있습니다.

```bash
jmeter
```

또는

```bash
open /usr/local/bin/jmeter
```

### Windows에 JMeter 설치

* [Download](https://jmeter.apache.org/download_jmeter.cgi) 페이지에서 apache-jmeter-5.4.1.zip 파일을 다운로드 받습니다.
* 압축을 풀고 원하는 경로로 옮깁니다. (예: D:\Dev\Tools\apache-jmeter-5.4.1)

버전을 확인합니다.

```bash
D:\Dev\Tools\apache-jmeter-5.4.1\bin>jmeter -v
    _    ____   _    ____ _   _ _____       _ __  __ _____ _____ _____ ____
   / \  |  _ \ / \  / ___| | | | ____|     | |  \/  | ____|_   _| ____|  _ \
  / _ \ | |_) / _ \| |   | |_| |  _|    _  | | |\/| |  _|   | | |  _| | |_) |
 / ___ \|  __/ ___ \ |___|  _  | |___  | |_| | |  | | |___  | | | |___|  _ <
/_/   \_\_| /_/   \_\____|_| |_|_____|  \___/|_|  |_|_____| |_| |_____|_| \_\ 5.4.1

Copyright (c) 1999-2021 The Apache Software Foundation
```

[Your JMeter Path]\bin\jmeter.bat를 더블클릭하여 **JMeter GUI**를 실행할 수 있습니다. (예: D:\Dev\Tools\apache-jmeter-5.4.1\bin\jmeter.bat)

## JMeter 구성 및 Test Plan 작성

### Authorization header 확인

* 로그인한 후, 브라우저의 개발자 도구를 엽니다.
* **Network** 탭으로 이동하고 **XHR**를 클릭합니다.
* 요청 목록 중 하나(예: `projects?limit=5&offset=0`)를 선택합니다.
* **Headers** 탭에서 **Request Headers** 중 **Authorization** header의 토큰(Bearer 제외한 문자열)을 복사한 다음, 기록해 둡니다.

![Authorization header](images/06/request_headers_authorization.png "Authorization header")

### JMeter GUI를 사용하여 Test Plan 작성

* **File > New**을 선택합니다.
* **Name** 필드에 `AccuInsight+ API Test Plan`을 입력합니다.
* 사이드 바에서 `AccuInsight+ API Test Plan`를 선택하고 우클릭한 다음, **Add > Config Element > User Defined Variables**를 선택합니다.
* **User Defined Variables**에서 화면 하단의 **Add** 버튼을 클릭한 다음, 아래 사용자 정의 변수를 추가합니다.
  * **Name** : `threads`, **Value** : `5`, **Description** : `Number of Threads`
  * **Name** : `ramp_up_period`, **Value** : `1`, **Description** : `Ramp-up period`
  * **Name** : `loops`, **Value** : `2`, **Description** : `Loop Count`
  * **Name** : `host`, **Value** : `<Server Domain or IP>`, **Description** : `Server Host`
  * **Name** : `port`, **Value** : `80`, **Description** : `Server Port`
  * **Name** : `auth_token`, **Value** : `<이전 단계에서 기록해 두었던 토큰>`, **Description** : `Auth Token`

    ![User Defined Variables](images/06/jmeter_user_defined_variables.png "User Defined Variables")

* 사이드 바에서 `AccuInsight+ API Test Plan`를 선택하고 우클릭한 다음, **Add > Thread(users) > Thread Group**를 선택합니다.
* **Thread Group**에서 다음 항목을 입력합니다.
  * **Number of threads (users)** : `${__P(threads,${threads})}`
  * **Ramp-up period (seconds)** : `${__P(ramp_up_period,${ramp_up_period})}`
  * **Loop Count** : `${__P(loops,${loops})}`

    ![Thread Group](images/06/jmeter_thread_group.png "Thread Group")

* 사이드 바에서 `Thread Group`를 선택하고 우클릭한 다음, **Add > Config Element > HTTP Header Manager**을 선택합니다.
* **Http Header Manager**에서 화면 하단의 **Add** 버튼을 클릭한 다음, Header의 이름과 값을 추가합니다.
  * **Name** : `Authorization`, **Value** : `Bearer ${__P(auth_token,${auth_token})}`

    ![Http Header Manager](images/06/jmeter_http_header_manager.png "Http Header Manager")

* 사이드 바에서 `Thread Group`를 선택하고 우클릭한 다음, **Add > Sampler > Http Request**을 선택합니다.
* **Http Request**에서 다음 항목을 입력합니다.
  * **Name** : `projects`
  * **Server Name or IP** : `${__P(host,${host})}`
  * **Port Number** : `${__P(port,${port})}`
  * **Path** : `/api/common/backend/projects`
  * **Parameters** : 화면 하단의 **Add** 버튼을 클릭한 다음, 파라미터를 추가합니다.
    * **Name** : `limit`, **Value** : `5`
    * **Name** : `offset`, **Value** : `0`
* 다시 사이드 바에서 `Thread Group`를 선택하고 우클릭한 다음, **Add > Sampler > Http Request**을 선택합니다.
* **Http Request**에서 다음 항목을 입력합니다.
  * **Name** : `workflows`
  * **Server Name or IP** : `${__P(host,${host})}`
  * **Port Number** : `${__P(port,${port})}`
  * **Path** : `/api/common/backend/projects/workflows`
  * **Parameters** : 화면 하단의 **Add** 버튼을 클릭한 다음, 파라미터를 추가합니다.
    * **Name** : `limit`, **Value** : `5`
    * **Name** : `offset`, **Value** : `0`
* 필요하다면 앞의 두 단계를 반복해서 **Http Request**를 추가합니다.

    ![Http Request](images/06/jmeter_http_request.png "Http Request")

* `Thread Group`를 선택하고 우클릭한 다음, **Add > Listener > View Results Tree 또는 View Results in Table**를 선택하여 테스트 결과를 볼 수 있습니다.
* **Disk** 아이콘을 클릭하고 `.jmx` 확장자로 이름을 지정하고 저장합니다. (ex: aiip-api-test-plan.jmx)
* 녹색 **Start 아이콘**을 클릭하여 테스트를 실행하고 결과를 확인합니다.

    ![View Results in Table](images/06/jmeter_view_results_in_table.png "View Results in Table")

## Test Plan 실행 및 Report Dashboard 생성

### CLI를 사용하여 JMeter Test Plan 실행

최적의 테스트 결과를 얻으려면 CLI (Non-GUI) 모드에서 JMeter를 실행해야 합니다.  
아래 명령 옵션을 사용할 수 있습니다.

* **-n** : JMeter가 CLI 모드에서 실행되도록 지정
* **-t** : [Test Plan을 포함하는 JMX 파일의 이름]
* **-l** : [샘플 결과를 기록할 JTL 파일의 이름]
* **-j** : [JMeter 실행 로그 파일의 이름]
* **-r** : JMeter 프로퍼티 "remote_hosts"로 지정된 서버에서 테스트를 실행
* **-R** : [원격 서버 목록] 지정된 원격 서버에서 테스트 실행
* **-g** : [CSV 파일 경로] 보고서 대시보드만 생성
* **-e** : 부하 테스트 후 보고서 대시보드 생성
* **-o** : 부하 테스트 후 보고서 대시보드를 생성할 출력 폴더 (폴더가 존재하지 않거나 비어 있어야 함)

아래 명령을 실행하면 JMeter Command 사용법을 확인할 수 있습니다.

```bash
$ jmeter -h


To run Apache JMeter in NON_GUI mode:
Open a command prompt (or Unix shell) and type:

jmeter.bat(Windows)/jmeter.sh(Linux) -n -t test-file [-p property-file] [-l results-file] [-j log-file]
```

Test Plan 파일((ex: aiip-api-test-plan.jmx))을 저장한 경로로 이동한 후, 아래 명령을 실행합니다.

```bash
$ jmeter -n -t aiip-api-test-plan.jmx -l aiip-api-report.jtl

WARNING: package sun.awt.X11 not in java.desktop
Creating summariser <summary>
Created the tree successfully using aiip-api-test-plan.jmx
Starting standalone test @ Fri Jul 16 12:26:47 KST 2021 (1626406007245)
Waiting for possible Shutdown/StopTestNow/HeapDump/ThreadDump message on port 4445
summary =     20 in 00:00:01 =   17.3/s Avg:    85 Min:    33 Max:   178 Err:     0 (0.00%)
Tidying up ...    @ Fri Jul 16 12:26:48 KST 2021 (1626406008464)
... end of run
```

**User Defined Variables**에서 추가한 사용자 정의 변수를 커맨드 라인(command line)에서 재정의할 수 있습니다.
`-q property-file` 옵션을 사용하여 Properties 파일을 지정하거나 `-J[prop_name]=[value]`으로 변수를 지정할 수 있습니다.

아래와 같이 실행합니다.

```bash
$ jmeter -n -t aiip-api-test-plan.jmx -l aiip-api-report.jtl -Jthreads=10 -Jramp_up_period=1 -Jloops=2 -Jhost=<Server Domain or IP> -Jport=80 -Jauth_token=<Your Auth token>

WARNING: package sun.awt.X11 not in java.desktopCreating summariser <summary>Created the tree successfully using aiip-api-test-plan.jmx
Starting standalone test @ Fri Jul 16 15:22:39 KST 2021 (1626416559437)
Waiting for possible Shutdown/StopTestNow/HeapDump/ThreadDump message on port 4445
summary =     40 in 00:00:02 =   25.8/s Avg:   162 Min:    34 Max:   449 Err:     0 (0.00%)
Tidying up ...    @ Fri Jul 16 15:22:41 KST 2021 (1626416561041)
... end of run
```

### HTML Report Dashboard 생성

JMeter는 테스트 계획에서 그래프와 통계를 얻기 위해 대시보드 보고서 생성을 지원합니다.

대시보드 생성기의 기본 동작은 CSV 파일에서 샘플을 읽고 처리하여 그래프 보기가 포함된 HTML 파일을 생성하는 것입니다. 부하 테스트가 끝날 때 또는 요청 시 보고서를 생성할 수 있습니다.

기존 샘플 CSV(또는 JTL) 로그 파일에서 생성하려면 :

```bash
jmeter -g <log file> -o <Path to output folder>
```

부하 테스트 후 생성하려면 :

```bash
jmeter -n -t <test JMX file> -l <test log file> -e -o <Path to output folder>
```

#### 대시보드 생성 구성

대시보드 생성은 JMeter 프로퍼티(properties)를 사용하여 보고서를 사용자 정의합니다. 일부 프로퍼티는 일반 설정에 사용되고 다른 프로퍼티는 특정 그래프 구성 또는 내보내기 구성에 사용됩니다.

> 모든 보고서 생성기 프로퍼티는 **reportgenerator.properties** 파일에서 찾을 수 있습니다. 이러한 프로퍼티를 사용자 지정하려면, **user.properties** 파일에서 복사하여 수정해야 합니다.  
> **reportgenerator.properties**, **user.properties** 파일은 아래 경로에 있습니다.  
>   
> * **macOS** : `/usr/local/Cellar/jmeter/5.4.1/libexec/bin`  
> * **Windows** : `D:\Dev\Tools\apache-jmeter-5.4.1\bin`  

**user.properties** 파일을 Test Plan (JMX) 파일이 있는 경로에 복사한 후, 아래 항목을 수정합니다.

* **jmeter.reportgenerator.report_title** : 생성되는 보고서 대시보드의 제목. 기본값은 `Apache JMeter Dashboard`
* **jmeter.reportgenerator.overall_granularity** : 시간 경과에 따른 그래프의 세분성. 이 값은 1초(또는 1000ms)보다 작아서는 안 됩니다. 짧은 기간 테스트의 경우 그래프와 오류가 올바르게 표시되도록 최소 세분성 값, 즉 1000(1000ms 또는 1초를 나타냄)을 유지하는 것이 좋습니다. 만약에 더 긴 내구 시험을 한다면 60000을 유지할 수 있습니다. 기본값은 `60000 (1분)`으로, 데이터는 1분 틱으로 집계됩니다.
* **jmeter.reportgenerator.apdex_satisfied_threshold** : APDEX(애플리케이션 성능 지수) 계산에 대한 만족 임계값 (ms 단위). 기본값은 500
* **jmeter.reportgenerator.apdex_tolerated_threshold** : APDEX 계산에 대한 허용 한계 임계값 (ms 단위). 기본값은 1500
* **jmeter.reportgenerator.exporter.html.series_filter** : 보고서에 표시하려는 트랜잭션의 이름. 테스트 계획에서 트랜잭션 컨트롤러 이름을 가져오는 가장 쉬운 방법은 JMeter 메뉴의 **Tools > Export transactions for report**를 선택한 후, 내용을 전체 복사하여 붙여넣는 것입니다. 보고서에 일부 트랜잭션 컨트롤러를 포함하지 않으려면 라인에서 제거합니다. 예를들어, 아래 설정에서 `workflows`를 제거하여 `jmeter.reportgenerator.exporter.html.series_filter=^(projects)(-success|-failure)?$`로 설정하면 **projects** 트랜잭션만 표시됩니다.

```properties
#---------------------------------------------------------------------------
# Reporting configuration
#---------------------------------------------------------------------------

# Configure this property to change the report title
jmeter.reportgenerator.report_title=AccuInsight+ JMeter Dashboard

# Used to generate a report based on a date range if needed
# Default date format (from SimpleDateFormat Java API and Locale.ENGLISH)
#jmeter.reportgenerator.date_format=yyyyMMddHHmmss
# Date range start date using date_format property
#jmeter.reportgenerator.start_date=
# Date range end date using date_format property
#jmeter.reportgenerator.end_date=

# Change this parameter if you want to change the granularity of over time graphs.
# Set to 60000 ms by default
jmeter.reportgenerator.overall_granularity=1000

# Change this parameter if you want to change the granularity of Response time distribution
# Set to 100 ms by default
#jmeter.reportgenerator.graph.responseTimeDistribution.property.set_granularity=100

# Change this parameter if you want to keep only some samples.
# Regular Expression which Indicates which samples to keep for graphs and statistics generation.
# Empty value means no filtering
#jmeter.reportgenerator.sample_filter=

# Change this parameter if you want to override the APDEX satisfaction threshold.
# Set to 500 ms by default
jmeter.reportgenerator.apdex_satisfied_threshold=500

# Change this parameter if you want to override the APDEX tolerance threshold.
# Set to 1500 ms by default
jmeter.reportgenerator.apdex_tolerated_threshold=1500

# Indicates which graph series are filtered (regular expression)
# In the below example we filter on Search and Order samples
# Note that the end of the pattern should always include (-success|-failure)?$
# TransactionsPerSecondGraphConsumer suffixes transactions with "-success" or "-failure" depending
# on the result
#jmeter.reportgenerator.exporter.html.series_filter=^(Search|Order)(-success|-failure)?$
jmeter.reportgenerator.exporter.html.series_filter=^(projects|workflows)(-success|-failure)?$

# Indicates whether only controller samples are displayed on graphs that support it.
#jmeter.reportgenerator.exporter.html.show_controllers_only=false

# This property is used by menu item "Export transactions for report"
# It is used to select which transactions by default will be exported
#jmeter.reportgenerator.exported_transactions_pattern=[a-zA-Z0-9_\\-{}\\$\\.]*[-_][0-9]*
```

#### CLI 모드에서 보고서 대시보드 생성

아래 명령을 실행하여 보고서 대시보드를 생성합니다.

```bash
jmeter -n -t aiip-api-test-plan.jmx -l aiip-api-report.jtl -e -o reports
```

reports 폴더에 있는 `index.html` 파일을 브라우저로 열면, 아래와 같이 테스트 통계와 그래프가 포함된 보고서 대시보드를 볼 수 있습니다.

![JMeter Report Dashboard](images/06/jmeter_report_dashboard.png "JMeter Report Dashboard")

## GitLab CI 파이프라인 구성

### GitLab 프로젝트 생성 및 파일 업로드

Test Plan (JMX) 파일 및 대시보드 구성 파일(`user.properties`)를 저장하고 CI 파이프라인을 구성하기 위해 [예제](https://gitlab.com/skdt/devopsguide/examples_cicd/performance-testing-with-jmeter)와 같이 GitLab 프로젝트를 생성합니다.

다음을 수행하여 필요한 파일을 업로드 합니다.

* **Project overview** 페이지에서 **Web IDE** 버튼을 클릭합니다.
* **Web IDE**의 **Edit** 페이지에서 왼쪽 사이드 바의 **New directory** 아이콘을 클릭합니다.
* **Create new directory** 모달 창의 **Name** 필드에 `.jmeter`를 입력하고 **Create directory** 버튼을 클릭합니다.
* **.jmeter** 디렉토리를 선택한 후, 오른쪽의 **⋮** 아이콘을 선택하고 **Upload file**을 선택하여 이전 단계에서 저장한 Test Plan (예: `aiip-api-test-plan.jmx`) 파일을 업로드 합니다.
* 파일에서 `auth_token`의 `Argument.value`의 내용을 제거합니다.

    ```xml
            <elementProp name="auth_token" elementType="Argument">
                <stringProp name="Argument.name">auth_token</stringProp>
                <stringProp name="Argument.value"></stringProp>
                <stringProp name="Argument.desc">Auth Token</stringProp>
                <stringProp name="Argument.metadata">=</stringProp>
            </elementProp>
    ```

* 다시 사이드 바에서 **.jmeter** 디렉토리를 선택한 후, 오른쪽의 **⋮** 아이콘을 선택하고 **Upload file**을 선택하여 `user.properties` 파일을 업로드 합니다.
* **Commit...** 버튼을 클릭한 다음, `Commit to main branch`를 선택하고 **Commit** 버튼을 클릭합니다.

### GitLab CI/CD 환경 변수 생성

`.gitlab-ci.yml` 파일에서 호출할 환경 변수 `THREADS`, `RAMP_UP_PERIOD`, `LOOPS`, `HOST`, `PORT`, `AUTH_TOKEN`를 생성합니다.

* 프로젝트의 **Settings > CI/CD**로 이동하여 **Variables** 섹션을 확장합니다.
* **Add Variable** 버튼을 클릭한 다음, **Add Variable** 모달 창에서 세부사항 입력하고 **Add variable** 버튼을 클릭합니다.
  * Key : `AUTH_TOKEN` 입력
  * Value : 이전 단계에서 생성하고 기록해 두었던 Auth token을 입력
  * Type : `Variable` 선택
  * Environment scope : `All` 선택
  * Protect variable : 체크
  * Mask variable : 체크

* 마찬가지 방법으로  `THREADS`, `RAMP_UP_PERIOD`, `LOOPS`, `HOST`, `PORT` 변수도 생성합니다. (Mask variable : 체크하지 않음)

![GitLab Variables](images/06/gitlab_variables.png "GitLab Variables")

### `.gitlab-ci.yml` 파일 생성

* GitLab의 **Project overview** 페이지에서 project slug(`performance-testing-with-jmeter`) 오른쪽에 있는 **[+]** 아이콘을 클릭하고 **New file**를 선택합니다.
* **File name** 필드에 `.gitlab-ci.yml`를 입력합니다.
* 아래 내용을 복사하여 붙여넣습니다.

    ```yaml
    load-test:
    stage: test
    image: 
        name: justb4/jmeter:latest
        entrypoint: [""]
    script:
        - mkdir reports
        - /entrypoint.sh -n -t .jmeter/aiip-api-test-plan.jmx -l aiip-api-report.jtl -q .jmeter/user.properties -Jthreads=${THREADS} -Jramp_up_period=${RAMP_UP_PERIOD} -Jloops=${LOOPS} -Jhost=${HOST} -Jport=${PORT} -Jauth_token=${AUTH_TOKEN} -e -o ./reports
    artifacts:
        paths:
        - reports

    pages:
    stage: deploy
    script:
        - mv reports public
    artifacts:
        paths:
        - public
    dependencies:
        - load-test
    ```

* **Commit message** 필드에 커밋 메시지를 입력합니다. (예: `Add initial .gitlab-ci.yml`)
* **Commit changes** 버튼을 클릭합니다.

### CI 파이프라인 결과 확인

* 사이드 바에서 **CI/CD > Pipelines**을 클릭하면 CI 파이프라인이 실행되는 것을 확인할 수 있습니다.
* 파이프라인 일련번호를 클릭하면 파이프라인 그래프를 볼 수 있습니다.
* `load-test` Job을 클릭하면 아래와 유사한 로그를 확인할 수 있습니다.

```bash
Running with gitlab-runner 13.12.0 (7a6612da)
  on skdt-group-runner-1 TNcP22Fe
Resolving secrets
00:00
Preparing the "docker" executor
Using Docker executor with image justb4/jmeter:latest ...
Pulling docker image justb4/jmeter:latest ...
Using docker image sha256:62f762a118ed97a9adb7b88243f3b61bba1851216ba44f15b401de531fb08495 for justb4/jmeter:latest with digest justb4/jmeter@sha256:5c058de788473333c303fdb1169e8c7a65bfafc237da57444c5f54e64954d245 ...
Preparing environment
00:06
Running on runner-tncp22fe-project-28218165-concurrent-0 via d569e09c61e5...
Getting source from Git repository
00:02
Fetching changes with git depth set to 50...
Initialized empty Git repository in /builds/skdt/devopsguide/examples_cicd/performance-testing-with-jmeter/.git/
Created fresh repository.
Checking out 2239bf97 as main...
Skipping Git submodules setup
Executing "step_script" stage of the job script
00:05
Using docker image sha256:62f762a118ed97a9adb7b88243f3b61bba1851216ba44f15b401de531fb08495 for justb4/jmeter:latest with digest justb4/jmeter@sha256:5c058de788473333c303fdb1169e8c7a65bfafc237da57444c5f54e64954d245 ...
$ mkdir reports
$ /entrypoint.sh -n -t .jmeter/aiip-api-test-plan.jmx -l aiip-api-report.jtl -q .jmeter/user.properties -Jthreads=${THREADS} -Jramp_up_period=${RAMP_UP_PERIOD} -Jloops=${LOOPS} -Jhost=${HOST} -Jport=${PORT} -Jauth_token=${AUTH_TOKEN} -e -o ./reports
START Running Jmeter on Sun Jul 18 06:09:26 CEST 2021
JVM_ARGS=-Xmn4172m -Xms16688m -Xmx16688m
jmeter args=-n -t .jmeter/aiip-api-test-plan.jmx -l aiip-api-report.jtl -q .jmeter/user.properties -Jthreads=5 -Jramp_up_period=1 -Jloops=2 -Jhost=aiip-stage.skcc.com -Jport=80 -Jauth_token=[MASKED] -e -o ./reports
Jul 18, 2021 6:09:26 AM java.util.prefs.FileSystemPreferences$1 run
INFO: Created user preferences directory.
Creating summariser <summary>
Created the tree successfully using .jmeter/aiip-api-test-plan.jmx
Starting standalone test @ Sun Jul 18 06:09:27 CEST 2021 (1626581367301)
Waiting for possible Shutdown/StopTestNow/HeapDump/ThreadDump message on port 4445
summary =     20 in 00:00:01 =   16.8/s Avg:    90 Min:    26 Max:   265 Err:     0 (0.00%)
Tidying up ...    @ Sun Jul 18 06:09:28 CEST 2021 (1626581368837)
... end of run
END Running Jmeter on Sun Jul 18 06:09:30 CEST 2021
Uploading artifacts for successful job
00:04
Uploading artifacts...
reports: found 156 matching files and directories  
Uploading artifacts as "archive" to coordinator... ok  id=1432716285 responseStatus=201 Created token=yDBPbjNN
Cleaning up file based variables
00:01
Job succeeded
```

### 보고서 대시보드 확인

사이드 바에서 **CI/CD > Pages**을 클릭하면 프로젝트의 GitLab Pages 접속 URL을 확인할 수 있습니다.  
링크를 클릭하면 보고서 대시보드에 접속할 수 있습니다.

또한, `load-test` Job 또는 `pages` Job의 artifacts에서 **Browse** 버튼을 클릭한 후, `reports` 또는 `public` 디렉토리의 `index.html`를 클릭하여 보고서 대시보드에 접속할 수 있습니다.

![GitLab Job artifacts - Browse](images/06/gitlab_job_artifacts_browse.png "GitLab Job artifacts - Browse")
