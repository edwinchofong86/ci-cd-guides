# SonarQube 연동 가이드

본 가이드에서는 Java 기반 Spring Boot 애플리케이션의 버그(Bugs), 보안 취약점(Vulnerabilities), 코드 스멜(Code smells) 등을 감지하기 위해 SonarQube와 연동하여 코드의 정적 분석을 수행하는 GitLab CI 파이프라인을 구성하는 방법을 설명합니다.

## 사전 조건

- GitLab 및 SonarQube의 사용자 계정이 있어야 합니다.
- GitLab CI 파이프라인을 구성하려는 프로젝트에 대한 `Maintainer` 또는 `Owner` 액세스 권한이 있어야 합니다.
- SonarQube 계정에 `Execute Analysis`, `Create` 권한(Permissions)이 있어야 합니다.

## SonarQube Token 생성

코드 검사를 실행할 때 실제 SonarQube 사용자의 자격증명(ID, 패스워드)을 제공하지 않고 사용자 토큰을 사용하면 보안을 강화할 수 있습니다.

SonarQube에서 다음과 같이 수행하여 인증 Token을 생성합니다.

- SonarQube에 로그인한 다음, 우측 상단의 **프로필 아이콘**(영문 이니셜)을 클릭한 후 **My Account**를 선택합니다.
- **Security** 탭을 클릭한 다음, **Tokens** 섹션의 **Generate Tokens** 필드에 Token 이름을 입력하고 **Generate** 버튼을 클릭합니다.
- 생성된 Token이 화면에 나타나면 복사하여 기록해 둡니다.

![SonarQube Generate Token](images/01/sonarqube_generate_token.png "SonarQube Generate Token")

## GitLab 프로젝트 생성

애플리케이션의 코드베이스를 저장하고 CI 파이프라인을 구성하기 위한 GitLab 프로젝트를 생성합니다.

![GitLab new Project](images/01/gitlab_new_project.png "GitLab new Project")

- 상단 네비게이션 바에서 **[+]** 아이콘을 클릭하고 **New project**를 선택하여 개인 프로젝트를 생성하거나, 그룹에서 **New project** 버튼을 클릭하여 프로젝트 생성합니다.
- **Create new project** 페이지에서 **Create blank project**를 클릭합니다.
- **Create blank project** 페이지에서 아래 항목을 입력 또는 선택하고 **Create project** 버튼을 클릭합니다.
  - **Project name** : 프로젝트 이름 입력 (예: `SonarQube with Spring Boot`)
  - **Visibility Level** : `Private` 선택
  - **Initialize repository with a README** 체크

## Spring Boot 애플리케이션의 코드 베이스 준비

[예제 프로젝트](https://gitlab.com/skdt/devopsguide/examples_cicd/sonarqube-with-spring-boot)와 같은 Spring Boot 애플리케이션의 코드 베이스를 생성하여 GitLab Repository에 Push합니다.

> [Spring initializr](https://start.spring.io/) 웹 도구를 사용하면 Spring Boot 애플리케이션의 Maven 또는 Gradle 프로젝트를 쉽게 생성할 수 있습니다.

## Runner를 사용할 수 있는지 확인

GitLab에서 Runner는 CI/CD Job을 실행하는 에이전트입니다. GitLab 인스턴스의 모든 프로젝트에서 사용할 수 있는 공유 러너(Shared runners)를 포함하여 프로젝트에 사용할 수 있는 러너가 이미 있을 수 있습니다.

사용 가능한 러너를 보려면 :

- **Settings > CI/CD**로 이동하여 **Runners**를 확장합니다.

활성 상태인 러너가 하나 이상 있고 그 옆에 녹색 원이 있으면, Job을 처리할 수 있는 러너가 있는 것입니다.

UI의 **Runners** 페이지에 러너가 나열되지 않으면, 사용자 또는 시스템 관리자가 [GitLab Runner를 설치](https://docs.gitlab.com/runner/install/)하고 하나 이상의 [러너를 등록](https://docs.gitlab.com/runner/register/)해야 합니다.

![GitLab Runners](images/01/gitlab_runners.png "GitLab Runners")

## GitLab CI/CD 환경 변수 생성

`.gitlab-ci.yml` 파일에서 호출할 사용자 정의 환경 변수 `SONAR_HOST_URL`와 `SONAR_TOKEN`를 생성합니다.

GitLab UI 내에서 사용자 정의 환경 변수를 추가하거나 업데이트할 수 있습니다.

1. 프로젝트의 **Settings > CI/CD**로 이동하여 **Variables** 섹션을 확장합니다.
2. **Add Variable** 버튼을 클릭한 다음, **Add Variable** 모달 창에서 세부사항 입력하고 **Add variable** 버튼을 클릭합니다.
    - Key : `SONAR_TOKEN` 입력
    - Value : 이전 단계에서 생성하고 기록해 두었던 SonarQube 인증 Token을 입력
    - Type : `Variable` 선택
    - Environment scope : `All` 선택
    - Protect variable (선택 사항) : 보호된 브랜치 또는 태그에서 실행되는 파이프라인에서만 변수를 사용하려면 체크
    - Mask variable (선택 사항) : 체크

        > 체크하면 변수의 **Value**가 Job 로그에 마스킹됩니다. 값이 [마스킹 요구사항](https://docs.gitlab.com/ee/ci/variables/README.html#mask-a-cicd-variable)을 충족하지 않으면 변수가 저장되지 않습니다.

3. 마찬가지 방법으로  `SONAR_HOST_URL` 변수도 생성합니다.
    - Key : `SONAR_HOST_URL` 입력
    - Value : SonarQube 서버 URL 입력 (예: `https://sonar.skdt.io`)

변수가 생성된 후 편집 아이콘을 클릭하여 세부 정보를 업데이트할 수 있습니다.

![GitLab Environment variables](images/01/gitlab_environment_variables.png "GitLab Environment variables")

## `.gitlab-ci.yml` 파일 생성

`.gitlab-ci.yml` 파일은 GitLab CI/CD에 대한 특정 지침을 구성하는 YAML 파일입니다.

이 파일에서 다음을 정의합니다.

- 러너가 실행해야 하는 작업(Job)의 구조와 순서
- 특정 조건이 발생할 때 러너가 내려야 하는 결정

예를 들어, `master`를 제외한 브랜치에 커밋할 때 테스트 모음을 실행할 수 있습니다. `master`에 커밋하면 동일한 테스트 슈트(Test Suite)를 실행하고 애플리케이션도 게시하려고 합니다.

다음을 수행하여 `.gitlab-ci.yml` 파일을 생성합니다.

- GitLab UI의 **Project overview > Details** 페이지에서 project slug(`sonarqube-with-spring-boot`) 오른쪽에 있는 **[+]** 아이콘을 클릭하고 **New file**을 선택합니다.
- **File name** 필드에 `.gitlab-ci.yml`를 입력합니다.
- 아래 내용을 복사하여 붙여넣습니다.

    ```yaml
    variables:
      MAVEN_OPTS: "-Dhttps.protocols=TLSv1.2 -Dmaven.repo.local=$CI_PROJECT_DIR/.m2/repository -Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=WARN -Dorg.slf4j.simpleLogger.showDateTime=true -Djava.awt.headless=true"
      MAVEN_CLI_OPTS: "--batch-mode --errors --fail-at-end --show-version -DinstallAtEnd=true -DdeployAtEnd=true"

    image: maven:3.8.1-openjdk-11

    cache:
      paths:
        - .m2/repository

    stages:
      - build
      - verify

    maven-build:
      stage: build
      script: "mvn $MAVEN_CLI_OPTS clean package -DskipTests"
      artifacts:
        paths:
          - target/*.jar

    sonarqube-check:
      stage: verify
      variables:
        SONAR_USER_HOME: "${CI_PROJECT_DIR}/.sonar"  # Defines the location of the analysis task cache
        GIT_DEPTH: "0"  # Tells git to fetch all the branches of the project, required by the analysis task
      cache:
        key: "${CI_JOB_NAME}"
        paths:
          - ${SONAR_USER_HOME}/cache
      script:
        - |
          mvn $MAVEN_CLI_OPTS verify sonar:sonar \
            -Dsonar.qualitygate.wait=true \
            -Dsonar.host.url=${SONAR_HOST_URL} \
            -Dsonar.login=${SONAR_TOKEN}
      artifacts:
        paths:
          - target/site
      allow_failure: true
    ```

- **Commit message** 필드에 커밋 메시지를 입력합니다. (예: `Add initial .gitlab-ci.yml`)
- **Commit changes** 버튼을 클릭합니다.

## GitLab CI 파이프라인 결과 확인

- 사이드바에서 **CI/CD > Pipelines**를 클릭하면 CI 파이프라인이 실행되는 것을 확인할 수 있습니다.
- 파이프라인 일련번호를 클릭하면 파이프라인 그래프를 볼 수 있습니다.

    ![GitLab Pipeline graph](images/01/gitlab_pipeline_graph.png "GitLab Pipeline graph")

- `maven-build` Job을 클릭하여 로그를 확인합니다.

    ```bash
    (생략)

    Executing "step_script" stage of the job script
    00:04
    $ mvn clean package -DskipTests
    832 [INFO] Scanning for projects...
    1002 [INFO] 
    1002 [INFO] --------------------< com.example:hello-sonarqube >---------------------
    1003 [INFO] Building hello-sonarqube 0.0.1-SNAPSHOT
    1004 [INFO] --------------------------------[ jar ]---------------------------------
    1267 [INFO] 
    1267 [INFO] --- maven-clean-plugin:3.1.0:clean (default-clean) @ hello-sonarqube ---
    1376 [INFO] 
    1376 [INFO] --- jacoco-maven-plugin:0.8.7:prepare-agent (jacoco-initialize) @ hello-sonarqube ---
    1531 [INFO] argLine set to -javaagent:/builds/skdt/devopsguide/examples_cicd/sonarqube-with-spring-boot/.m2/repository/org/jacoco/org.jacoco.agent/0.8.7/org.jacoco.agent-0.8.7-runtime.jar=destfile=/builds/skdt/devopsguide/examples_cicd/sonarqube-with-spring-boot/target/jacoco.exec
    1532 [INFO] 
    1532 [INFO] --- maven-resources-plugin:3.2.0:resources (default-resources) @ hello-sonarqube ---
    1665 [INFO] Using 'UTF-8' encoding to copy filtered resources.
    1665 [INFO] Using 'UTF-8' encoding to copy filtered properties files.
    1668 [INFO] Copying 1 resource
    1679 [INFO] Copying 0 resource
    1680 [INFO] 
    1680 [INFO] --- maven-compiler-plugin:3.8.1:compile (default-compile) @ hello-sonarqube ---
    1767 [INFO] Changes detected - recompiling the module!
    1769 [INFO] Compiling 3 source files to /builds/skdt/devopsguide/examples_cicd/sonarqube-with-spring-boot/target/classes
    2364 [INFO] 
    2364 [INFO] --- maven-resources-plugin:3.2.0:testResources (default-testResources) @ hello-sonarqube ---
    2367 [INFO] Using 'UTF-8' encoding to copy filtered resources.
    2367 [INFO] Using 'UTF-8' encoding to copy filtered properties files.
    2367 [INFO] skip non existing resourceDirectory /builds/skdt/devopsguide/examples_cicd/sonarqube-with-spring-boot/src/test/resources
    2367 [INFO] 
    2368 [INFO] --- maven-compiler-plugin:3.8.1:testCompile (default-testCompile) @ hello-sonarqube ---
    2371 [INFO] Changes detected - recompiling the module!
    2372 [INFO] Compiling 2 source files to /builds/skdt/devopsguide/examples_cicd/sonarqube-with-spring-boot/target/test-classes
    2686 [INFO] 
    2686 [INFO] --- maven-surefire-plugin:2.22.2:test (default-test) @ hello-sonarqube ---
    2765 [INFO] Tests are skipped.
    2765 [INFO] 
    2765 [INFO] --- jacoco-maven-plugin:0.8.7:report (jacoco-site) @ hello-sonarqube ---
    2768 [INFO] Skipping JaCoCo execution due to missing execution data file.
    2769 [INFO] 
    2769 [INFO] --- maven-jar-plugin:3.2.0:jar (default-jar) @ hello-sonarqube ---
    2906 [INFO] Building jar: /builds/skdt/devopsguide/examples_cicd/sonarqube-with-spring-boot/target/hello-sonarqube-0.0.1-SNAPSHOT.jar
    2944 [INFO] 
    2944 [INFO] --- spring-boot-maven-plugin:2.5.0:repackage (repackage) @ hello-sonarqube ---
    3220 [INFO] Replacing main artifact with repackaged archive
    3220 [INFO] ------------------------------------------------------------------------
    3220 [INFO] BUILD SUCCESS
    3221 [INFO] ------------------------------------------------------------------------
    3222 [INFO] Total time:  2.406 s
    3222 [INFO] Finished at: 2021-05-26T10:54:28Z
    3223 [INFO] ------------------------------------------------------------------------
    Saving cache for successful job
    Creating cache default...
    .m2/repository: found 2633 matching files and directories 
    Archive is up to date!                             
    Created cache
    Uploading artifacts for successful job
    00:08
    Uploading artifacts...
    target/*.jar: found 1 matching files and directories 
    Uploading artifacts as "archive" to coordinator... ok  id=1294617224 responseStatus=201 Created token=k3rrUWXA
    Cleaning up file based variables
    00:00
    Job succeeded
    ```

- `sonarqube-check` Job을 클릭하여 로그를 확인합니다.

    ```bash
    (생략)

    Executing "step_script" stage of the job script
    $ mvn verify sonar:sonar \ # collapsed multi-line command
    839 [INFO] Scanning for projects...
    Downloading from central: https://repo.maven.apache.org/maven2/org/springframework/boot/spring-boot-starter-parent/2.5.0/spring-boot-starter-parent-2.5.0.pom
    Downloaded from central: https://repo.maven.apache.org/maven2/org/springframework/boot/spring-boot-starter-parent/2.5.0/spring-boot-starter-parent-2.5.0.pom (8.6 kB at 9.0 kB/s)

    (생략)

    2021-05-26 11:53:19.232  INFO 141 --- [           main] o.s.b.a.ApplicationAvailabilityBean      : Application availability state ReadinessState changed to ACCEPTING_TRAFFIC
    137616 [INFO] Tests run: 1, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.627 s - in com.example.demo.DemoApplicationTests
    138036 [INFO] 
    138036 [INFO] Results:
    138036 [INFO] 
    138036 [INFO] Tests run: 3, Failures: 0, Errors: 0, Skipped: 0
    138036 [INFO] 
    138045 [INFO] 
    138045 [INFO] --- jacoco-maven-plugin:0.8.7:report (jacoco-site) @ hello-sonarqube ---
    138056 [INFO] Loading execution data file /builds/skdt/devopsguide/examples_cicd/sonarqube-with-spring-boot/target/jacoco.exec
    138174 [INFO] Analyzed bundle 'hello-sonarqube' with 3 classes
    138241 [INFO] 
    138241 [INFO] --- maven-jar-plugin:3.2.0:jar (default-jar) @ hello-sonarqube ---
    Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/shared/file-management/3.0.0/file-management-3.0.0.pom

    (생략)

    196364 [INFO] Analysis report generated in 58ms, dir size=107 KB
    196380 [INFO] Analysis report compressed in 16ms, zip size=22 KB
    196570 [INFO] Analysis report uploaded in 190ms
    196571 [INFO] ------------- Check Quality Gate status
    196572 [INFO] Waiting for the analysis report to be processed (max 300s)
    201623 [INFO] QUALITY GATE STATUS: PASSED - View details on https://sonar.skdt.io/dashboard?id=com.example%3Ahello-sonarqube
    201629 [INFO] Analysis total time: 10.064 s
    201630 [INFO] ------------------------------------------------------------------------
    201630 [INFO] BUILD SUCCESS
    201630 [INFO] ------------------------------------------------------------------------
    201632 [INFO] Total time:  03:20 min
    201632 [INFO] Finished at: 2021-05-26T11:54:23Z
    201632 [INFO] ------------------------------------------------------------------------
    Saving cache for successful job
    Creating cache sonarqube-check...
    /builds/skdt/devopsguide/examples_cicd/sonarqube-with-spring-boot/.sonar/cache: found 49 matching files and directories 
    Uploading cache.zip to https://infograb-io-gitlab-runner.s3.dualstack.ap-northeast-2.amazonaws.com/runner/project/26932464/sonarqube-check 
    Created cache
    Uploading artifacts for successful job
    00:02
    Uploading artifacts...
    target/site: found 36 matching files and directories 
    Uploading artifacts as "archive" to coordinator... ok  id=1294789936 responseStatus=201 Created token=RCguhQAT
    Cleaning up file based variables
    00:00
    Job succeeded
    ```

## SonarQube에서 결과 확인

**SonarQube**에 접속한 다음, 상단 네비게이션 바에서 **Projects**를 클릭하면 아래와 같이 프로젝트 목록을 확인할 수 있습니다.

![SonarQube Projects](images/01/sonarqube_projects.png "SonarQube Projects")

프로젝트 ID를 클릭하면 상세 정보를 확인할 수 있습니다.

![SonarQube Project Overview](images/01/sonarqube_project_overview.png "SonarQube Project Overview")
