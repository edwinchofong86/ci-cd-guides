# GitLab Runner 설치 및 등록 가이드

본 가이드에서는 CentOS 7에 Docker Engine과 Docker Compose를 설치한 후, 이를 이용하여 Runner를 설치 및 시작하고 GitLab에 등록하는 방법을 설명합니다.

## Docker Engine 설치 및 구성

신규 Host Machine에 처음으로 Docker Engine을 설치하기 전에, Docker Repository 설정이 필요합니다. 이후에 Repository에서 Docker를 설치하고 업데이트할 수 있습니다.

### Repository 구성

yum 패키지를 업데이트하고 `yum-utils` 패키지(`yum-config-manager` 유틸리티를 제공하는)를 설치하고 Stable Repository(안정 버전 저장소)를 설정합니다.

```bash
sudo yum -y update
sudo yum install -y yum-utils
sudo yum-config-manager \
    --add-repo \
    https://download.docker.com/linux/centos/docker-ce.repo
```

### Docker Engine 설치

Docker Engine과 containerd 최신 버전을 설치합니다.

```bash
sudo yum install docker-ce docker-ce-cli containerd.io
```

Docker를 시작합니다.

```bash
sudo systemctl start docker
```

`hello-world` 이미지를 구동하여 Docker Engine가 정상적으로 설치되었는지 확인합니다.

```bash
sudo docker run hello-world
```

### sudo 없이 docker 명령어 사용하기

Docker daemon은 기본적으로 /var/run/docker.sock에서 생성된 unix domain socket(IPC socket)을 사용하여 통신하는데, root 권한이 있거나 사용자가 docker 그룹의 멤버이어야 합니다.

sudo 없이 docker 명령어를 사용하려면, “docker” 그룹에 사용자를 추가해야 합니다.

```bash
sudo usermod -aG docker $USER
```

로그아웃 후 SSH 재접속합니다.

```bash
exit
```

sudo 없이 docker 명령이 실행되는 것을 확인합니다.

```bash
$ docker ps -a
CONTAINER ID   IMAGE         COMMAND    CREATED         STATUS                     PORTS     NAMES
0f8752d2ebg7   hello-world   "/hello"   3 minutes ago   Exited (0) 3 minutes ago             romantic_hopper
```

아래 명령을 실행하여 hello-world 컨테이너를 삭제합니다.

```bash
$ docker rm 0f8752d2ebg7
0f8752d2ebg7

$ docker ps -a
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES
```

## Docker Compose 설치

[GitHub의 Compose repository 릴리즈 페이지](https://github.com/docker/compose/releases)에서 Docker Compose 바이너리를 다운로드 할 수 있습니다.

아래 명령을 실행하여 Docker Compose의 현재 안정 버전 릴리스(stable release)를 다운로드 합니다.

```bash
sudo curl \
    -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" \
    -o /usr/local/bin/docker-compose
```

> 다른 버전의 Compose를 설치하려면 `1.29.2`를 사용하려는 Compose 버전으로 대체 하십시오.

바이너리에 실행 권한을 적용합니다.

```bash
sudo chmod +x /usr/local/bin/docker-compose
```

정상 설치되었는지 확인합니다.

```bash
$ docker-compose --version
docker-compose version 1.29.2, build 1110ad01
```

## GitLab Runner 설치 및 구성

### GitLab Runner 설치 및 시작

Runner 작업 디렉토리 (Working directory)를 생성합니다.

```bash
sudo mkdir gitlab-runner
cd gitlab-runner
```

GitLab Runner 데이터를 영속적(Persistent)으로 저장하기 위한 바인드 마운트(Bind mount)용 디렉토리를 생성합니다.

```bash
sudo mkdir config
```

gitlab-runner 디렉토리의 소유권을 $USER로 변경하고 권한을 변경합니다.

```bash
sudo chown -R $USER:$USER /home/gitlab/gitlab-runner
```

docker-compose.yml 파일을 생성합니다.

```bash
cat <<EOF > docker-compose.yml
gitlab-runner:
  container_name: gitlab-runner
  image: 'gitlab/gitlab-runner:latest'
  restart: always
  volumes:
    - './config:/etc/gitlab-runner'
    - '/var/run/docker.sock:/var/run/docker.sock'
EOF
```

아래 명령을 실행하여 Runner를 시작합니다.

```bash
docker-compose up -d
```

### GitLab Runner 등록

GitLab UI에는 액세스 할 사용자에 따라 세 가지 유형의 러너가 있습니다.

* 공유 러너(Shared runner)는 자체 관리형 GitLab 인스턴스의 모든 그룹 및 프로젝트에서 사용할 수 있습니다. (Admin Area의 Overview > Runners)
* 그룹 러너(Group runner)는 그룹의 모든 프로젝트와 하위 그룹에서 사용할 수 있습니다. (그룹의 Settings > CI/CD > Runners 섹션)
* 특정 러너(Specific runner)는 특정 프로젝트와 연결됩니다. 일반적으로 특정 러너는 한 번에 하나의 프로젝트에 사용됩니다. (프로젝트의 Settings > CI/CD > Runners 섹션)

여기에서는 그룹 러너를 등록하는 방법을 설명합니다. (그룹에 대한 **Owner 권한**이 필요합니다.)

1. GitLab UI에서 러너가 작동하도록 할 그룹으로 이동합니다.
2. **Settings > CI/CD**로 이동하여 **Runners** 섹션을 확장합니다.
3. 터미널에서 아래 명령을 실행하여 `gitlab-runner` 컨네이너에 대화형(interactive) bash 셸을 실행합니다.

    ```bash
    docker-compose exec gitlab-runner bash
    ```

4. `gitlab-runner register` 명령을 실행하고 지침에 따라 아래 항목을 입력합니다.

   * **Enter the GitLab instance URL** : GitLab UI에서 Copy URL 아이콘을 클릭하여 클립보드에 복사한 후 붙여넣고 Enter 키를 누룹니다.
   * **Enter the registration token** : GitLab UI에서 Copy token 아이콘을 클릭하여 클립보드에 복사한 후 붙여넣고 Enter 키를 누룹니다.
   * **Enter a description for the runner** : 러너에 대한 설명을 입력하고 Enter 키를 누룹니다. (예: skdt-group-runner-1)
   * **Enter tags for the runner** : 아무것도 입력하지 않고 Enter 키를 누룹니다.
   * **Enter an executor** : docker을 입력하고 Enter 키를 누룹니다.
   * **Enter the default Docker image** : alpine:latest을 입력하고 Enter 키를 누룹니다.

5. GitLab UI를 새로고침하면 등록된 러너가 목록에 나타납니다.

### GitLab Runner 구성

GitLab Runner 및 등록된 개별 Runner의 동작을 변경할 수 있습니다.

GitLab Runner의 구성을 변경하려면 `config.toml` 파일을 수정해야 합니다.

대부분의 옵션을 변경할 때 GitLab Runner를 다시 시작할 필요가 없습니다. 여기에는 `listen_address`를 제외한 `[[runners]]` 섹션의 파라미터와 글로벌 섹션의 대부분의 파라미터가 포함됩니다.

GitLab Runner는 3 초마다 구성 수정사항을 확인하고 필요한 경우 다시 로드합니다.

#### Job 동시성(concurrency) 설정

GitLab Runner가 동시에 여러 Job을 실행할 수 있도록 적절하게 `concurrent`을 수정합니다.

예를 들어, 8vCPU/32GiB인 경우 `concurrent = 8`로 설정합니다.

GitLab.com의 [자동 확장(Auto-scaling) Shared Runner](https://docs.gitlab.com/ee/user/gitlab_com/index.html#shared-runners)는 단일 작업이 1 vCPU와 3.75GiB를 사용하여 단일 인스턴스에서 실행되도록 구성됩니다.

#### Docker 특권(privileged) 모드 설정

아래 CI 파이프라인(`.gitlab-ci.yml`)과 같이, [Docker-in-Docker](https://hub.docker.com/_/docker) 컨테이너를 사용하여 `docker build`와 같은 스크립트를 실행하기 위해서는 [특권 모드(privileged mode)](https://docs.docker.com/engine/reference/run/#runtime-privilege-and-linux-capabilities) 설정이 필요합니다.

```yaml
image: docker:git
services:
  - docker:dind

build:
  script:
    - docker build -t my-image .
    - docker push my-image
```

`[runners.docker]` 섹션에서 `privileged = true`로 설정합니다.

위에서 언급한 **Runner 옵션**을 수정하려면 Runner 작업 디렉토리(예: `/home/gitlab/gitlab-runner`)에서 아래 명령을 실행하고 수정합니다.

```bash
sudo vi config/config.toml
```

수정한 `config.toml` 파일은 아래와 유사합니다.

```toml
concurrent = 8
check_interval = 0

[session_server]
  session_timeout = 1800

[[runners]]
  name = "skdt-group-runner-1"
  url = "https://gitlab.com/"
  token = "Txxxxxxyyyyyyyyyx"
  executor = "docker"
  [runners.custom_build_dir]
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]
    [runners.cache.azure]
  [runners.docker]
    tls_verify = false
    image = "alpine:latest"
    privileged = true
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/cache"]
    shm_size = 0
```

### CentOS 7 서버 재시작시 Runner 자동 재시작 설정
서버가 재시작되는 상황에서 Docker 및 Runner가 자동으로 재시작되려면 다음 2가지가 설정되어 있어야 합니다.

1. Docker Service 자동시작
  - 서버가 재시작된 이후 Docker 서비스가 작동되는 중인지 확인
```bash
$ systemctl status docker
```
  - Docker 서비스 시작
```bash
$ systemctl start docker
````
  -  서버 부팅될 때 Docker 서비스 실행 설정
```bash
$ systemctl enable docker
```

2. Runner 자동 시작 설정
- runner의 docker-compose.yml 파일에 `restart: always` 옵션이 포함
