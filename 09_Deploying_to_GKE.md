# Google Kubernetes Engine(GKE) 배포 가이드

본 가이드에서는 Google Kubernetes Engine(GKE) 클러스터에 배포 환경을 구성하고 GitLab CD 파이프라인으로 GKE 클러스터에 애플리케이션을 배포하는 방법을 설명합니다.

## 사전 조건

* GitLab 프로젝트에 대한 `Maintainer` 또는 `Owner` 액세스 권한이 있어야 합니다.
* Google Cloud Platform(GCP)에 Kubernetes 클러스터가 생성되어 있어야 합니다.
* 로컬 PC에 [Google Cloud SDK](https://cloud.google.com/sdk/docs/quickstart?hl=ko)가 설치 및 구성되어 있어야 합니다.
* 로컬 PC에 [Helm](https://helm.sh/docs/intro/install/)이 설치되어 있어야 합니다.
* 로컬 PC에 [kubectl](https://kubernetes.io/ko/docs/tasks/tools/)이 설치되어 있어야 합니다.

## kubectl 사용자 인증 정보

터미널에서 아래 명령을 실행하여 `gcloud`가 Google 사용자 자격 증명을 사용하여 Google Cloud Platform에 액세스할 수 있도록 승인합니다.

```bash
gcloud auth login
```

`gcloud container clusters get-credentials <Cluster-Name> --zone=<Zone>` 명령을 실행하여 클러스터 인증 정보를 가져옵니다.

```bash
$ gcloud container clusters get-credentials gitlab-cicd-cluster --zone asia-northeast3-a
Fetching cluster endpoint and auth data.
kubeconfig entry generated for gitlab-cicd-cluster.
```

아래 명령을 실행하면 컨텍스트 목록 및 현재 컨텍스트를 확인할 수 있습니다.

```bash
kubectl config get-contexts     # 컨텍스트 리스트 출력
kubectl config current-context  # 현재 컨텍스트 출력
```

현재 컨텍스트가 배포할 클러스터가 아니면, 아래와 같이 변경합니다.

```bash
kubectl config use-context <your-cluster-name>
```

## NGINX Ingress controller 설치

아래 명령을 실행하여 `ingress-nginx`라는 이름의 Namespace를 생성합니다.

```bash
kubectl create namespace ingress-nginx
```

Namespace 목록을 확인합니다.

```bash
$ kubectl get namespaces
NAME              STATUS   AGE
default           Active   44h
ingress-nginx     Active   26s
kube-node-lease   Active   44h
kube-public       Active   44h
kube-system       Active   44h
```

현재 Namespace를 변경합니다.

```bash
kubectl config set-context --current --namespace=ingress-nginx
```

Helm을 사용하여 NGINX Ingress controller를 설치합니다.

```bash
helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
helm repo update

helm install gitlab-ingress-nginx ingress-nginx/ingress-nginx \
  --set controller.ingressClass=gitlab-ingress-nginx
```

아래와 같이 설치된 NGINX Ingress Controller를 확인할 수 있습니다.

```bash
kubectl get services
NAME                                        TYPE           CLUSTER-IP      EXTERNAL-IP     PORT(S)                      AGE
gitlab-ingress-nginx-controller             LoadBalancer   10.104.15.196   <pending>       80:32344/TCP,443:32108/TCP   27s
gitlab-ingress-nginx-controller-admission   ClusterIP      10.104.7.255    <none>          443/TCP                      27s
```

잠시 후 로드 밸런서가 IP 주소를 얻고, 다음 명령을 사용하여 외부 IP 주소를 가져올 수 있습니다.

```bash
kubectl get service gitlab-ingress-nginx-controller -o json | jq -r '.status.loadBalancer.ingress[].ip'
```

다음 단계에서 필요하므로 이 IP 주소를 **기록해 둡니다.**

## Kubernetes RBAC(Role-based Access Control) 구성

### Namespace 생성

아래 명령을 실행하여 `hello-ns`라는 이름의 Namespace를 생성합니다.

```bash
kubectl create namespace hello-ns
```

Namespace 목록을 확인합니다.

```bash
$ kubectl get namespaces
NAME              STATUS   AGE
default           Active   44h
hello-ns          Active   24s
ingress-nginx     Active   12m
kube-node-lease   Active   44h
kube-public       Active   44h
kube-system       Active   44h
```

현재 Namespace를 변경합니다.

```bash
kubectl config set-context --current --namespace=hello-ns
```

### Service Account 생성

`hello-sa`라는 이름의 Service Account를 생성합니다.

```bash
cat <<EOF | kubectl apply -f -
apiVersion: v1
kind: ServiceAccount
metadata:
  name: hello-sa
  namespace: hello-ns
EOF
```

Service Account 정보를 확인합니다.

```bash
kubectl get serviceaccounts hello-sa -o yaml
```

### Role 생성

`hello-role`라는 이름의 Role를 생성합니다.

```bash
cat <<EOF | kubectl apply -f -
apiVersion: rbac.authorization.k8s.io/v1
kind: Role
metadata:
  name: hello-role
  namespace: hello-ns
rules:
- apiGroups: ["extensions", "apps"]
  resources: ["deployments"]
  verbs: ["get", "list", "watch", "create", "update", "patch", "delete"]
- apiGroups: [""]
  resources: ["services", "endpoints", "pods", "secrets"]
  verbs: ["get", "list", "watch", "create", "update", "patch", "delete"]
- apiGroups: ["networking.k8s.io"]
  resources: ["ingresses"]
  verbs: ["get", "list", "watch", "create", "update", "patch", "delete"]
EOF
```

Role 정보를 확인합니다.

```bash
kubectl get roles hello-role -o yaml
```

### Role Binding 생성

`hello-rb`라는 이름의 RoleBinding를 생성하여 `hello-sa` Service Account와 `hello-role` Role를 Binding 합니다.

```bash
cat <<EOF | kubectl apply -f -
apiVersion: rbac.authorization.k8s.io/v1
kind: RoleBinding
metadata:
  name: hello-rb
  namespace: hello-ns
subjects:
- kind: ServiceAccount
  name: hello-sa
  namespace: hello-ns
roleRef:
  kind: Role 
  name: hello-role
  apiGroup: rbac.authorization.k8s.io
EOF
```

Role Binding 정보를 확인합니다.

```bash
kubectl get rolebindings hello-rb -o yaml
```

### Service Account Token으로 인증하는 kubeconfig 파일 생성

* 아래 명령을 실행하여 시크릿(Secret) 목록을 조회합니다.

    ```bash
    $ kubectl get secrets
    NAME                   TYPE                                  DATA   AGE
    default-token-4v978    kubernetes.io/service-account-token   3      5m35s
    hello-sa-token-w28bn   kubernetes.io/service-account-token   3      105s
    ```

* `hello-sa` Service Account의 토큰인 `hello-sa-token-w28bn`와 유사한 시크릿이 보여질 것 입니다.
* 아래 명령을 실행하여 Server URL을 확인합니다.

    ```bash
    kubectl cluster-info
    ```

* kubeconfig 파일을 생성하는데 필요한 변수를 설정합니다.

    ```bash
    CLUSTER_NAME=gke-gitlab-cluster     # 원하는 클러스터 이름 지정
    SERVER=<your-server-url>            # 위에서 확인한 서버 주소 (예: https://172.64.5.11)
    NAMESPACE=hello-ns
    SERVICE_ACCOUNT=hello-sa
    SA_TOKEN_NAME=hello-sa-token-w28bn  # 위에서 확인한 Service Account의 토큰
    CA=$(kubectl get secrets $SA_TOKEN_NAME -o jsonpath='{.data.ca\.crt}')
    TOKEN=$(kubectl get secrets $SA_TOKEN_NAME -o jsonpath='{.data.token}' | base64 -d)
    ```

* 아래와 같이 실행하여 kubeconfig 파일을 생성합니다.

    ```bash
    echo "
    apiVersion: v1
    kind: Config
    clusters:
      - name: ${CLUSTER_NAME}
        cluster:
          certificate-authority-data: ${CA}
          server: ${SERVER}
    contexts:
      - name: ${CLUSTER_NAME}
        context:
          cluster: ${CLUSTER_NAME}
          namespace: ${NAMESPACE}
          user: ${SERVICE_ACCOUNT}
    users:
      - name: ${SERVICE_ACCOUNT}
        user:
          token: ${TOKEN}
    current-context: ${CLUSTER_NAME}
    " > sa_kube_config.yaml
    ```

* 생성한 `sa_kube_config.yaml` kubeconfig 파일로 `default` Namespace의 Pods를 조회하면 권한이 없는 것을 확인할 수 있습니다.

    ```bash
    $ kubectl --kubeconfig sa_kube_config.yaml -n default get pods
    Error from server (Forbidden): pods is forbidden: User "system:serviceaccount:hello-ns:hello-sa" cannot list resource "pods" in API group "" in the namespace "default"
    ```

* `hello-ns` Namespace의 Pods를 조회하면 권한이 있는 것을 확인할 수 있습니다.

    ```bash
    kubectl --kubeconfig sa_kube_config.yaml -n hello-ns get pods
    No resources found in hello-ns namespace.
    ```

## GitLab CD 파이프라인 구성

CD 파이프라인을 구성하기 위해 [Google Kubernetes Engine(GKE) CD 파이프라인 예제](https://gitlab.com/skdt/devopsguide/examples_cicd/deploy-to-gke)와 같이 GitLab 프로젝트를 생성하고 코드베이스를 Push 합니다.

### 개인 엑세스 토큰 생성

Kubernetes에서 파드(Pod)를 생성하려면 Private Docker 레지스트리(GitLab Container Registry)로부터 이미지를 받아오기 위해 사용하는 시크릿(Secret)이 필요합니다.  
이러한 시크릿을 `imagePullSecret`이라고 하며, 아래 명령으로 생성할 수 있습니다.

```bash
kubectl create secret docker-registry image-pull-secret \
  --docker-server=<your-registry-server> \
  --docker-username=<your-name> \
  --docker-password=<your-pword> \
  --docker-email=<your-email>
```

`docker-password`에 사용할 엑세스 토큰을 생성합니다.

* GitLab에서 우측 상단에 있는 아바타를 클릭하고 **Edit profile**을 선택합니다.
* **User Settings** 페이지의 좌측 사이드 바에서 **Access Tokens**를 클릭합니다.
* **Token name** 필드에 토큰의 이름을 입력합니다. (예: `registry-token`)
* **Expiration date** 필드에 만료 날짜를 선택합니다. (선택사항)
* **Select scopes**에서 `read_registry` 체크박스를 체크하고 **Create personal access token** 버튼을 클릭합니다.
* **Your new personal access token** 필드에 생성된 액세스 토큰이 표시됩니다.
* **Copy personal access token** 아이콘을 클릭하여 복사하고, **액세스 토큰을 기록해 둡니다.**
  > **페이지를 나가거나 새로 고침하면 다시 액세스 할 수 없습니다.**

### GitLab CI/CD 환경 변수 생성

`.gitlab-ci.yml` 파일에서 호출할 환경 변수 `KUBECONFIG`, `KUBE_INGRESS_BASE_DOMAIN`, `DOCKER_USERNAME`, `DOCKER_PASSWORD`, `DOCKER_EMAIL`를 생성합니다.

* 프로젝트의 **Settings > CI/CD**로 이동하여 **Variables** 섹션을 확장합니다.
* **Add Variable** 버튼을 클릭한 다음, **Add Variable** 모달 창에서 세부사항 입력하고 **Add variable** 버튼을 클릭합니다.
  * Key : `KUBECONFIG` 입력
  * Value : 이전 단계에서 생성한 `sa_kube_config.yaml` kubeconfig 파일 내용를 복사하여 붙여넣음
  * Type : `File` 선택
  * Environment scope : `All` 선택
  * Protect variable : 체크 해제
  * Mask variable : 체크 해제
* `KUBE_INGRESS_BASE_DOMAIN` 변수를 생성합니다.
  * Key : `KUBE_INGRESS_BASE_DOMAIN` 입력
  * Value : 이전 단계에서 기록해 두었던 NGINX Ingress controller의 로드 밸런서 IP 주소를 `<IP address>.nip.io`와 같은 형태로 입력 (예: `172.64.5.11.nip.io`)
    > [nip.io](https://nip.io/)는 무료 wildcard DNS 서비스로, 서비스 IP 주소와 유사한 형태의 도메인을 제공해 줍니다.  
    > 예) 192.168.1.250.nip.io, 192-168-1-250.nip.io, app-192-168-1-250.nip.io
  * Type : `Variable` 선택
  * Protect variable : 체크 해제
* `DOCKER_USERNAME` 변수를 생성합니다.
  * Key : `DOCKER_USERNAME` 입력
  * Value : 본인의 GitLab 계정 입력 (예: `gdhong123`)
  * Type : `Variable` 선택
  * Protect variable : 체크 해제
* `DOCKER_PASSWORD` 변수를 생성합니다.
  * Key : `DOCKER_PASSWORD` 입력
  * Value : 이전 단계에서 기록해 두었던 액세스 토큰 입력
  * Type : `Variable` 선택
  * Protect variable : 체크 해제
  * Mask variable : 체크
* `DOCKER_EMAIL` 변수를 생성합니다.
  * Key : `DOCKER_EMAIL` 입력
  * Value : 본인의 이메일 입력
  * Type : `Variable` 선택
  * Protect variable : 체크 해제

![GitLab Environment variables](images/09/gitlab_environment_variables.png "GitLab Environment variables")

### Dockerfile 파일 생성

애플리케이션의 Docker 이미지를 생성하는데 필요한 `Dockerfile`, `.dockerignore` 파일을 생성합니다.

* **Project 랜딩 페이지**에서 **Web IDE** 버튼을 클릭합니다.
* **Web IDE** 페이지의 왼쪽 사이드 바에서 **New file** 아이콘을 클릭합니다.
* **Create new file** 모달 창의 **Name** 필드에 `Dockerfile`를 입력하고 **Create file** 버튼을 클릭합니다.
* 아래 내용을 복사하여 붙여넣습니다.

    ```dockerfile
    FROM node:14.17.3-alpine
    WORKDIR /usr/src/app
    COPY package*.json ./
    RUN npm install --silent
    COPY . .
    EXPOSE 3000
    CMD [ "npm", "start" ]
    ```

* 다시 사이드 바에서 **New file** 아이콘을 클릭합니다.
* **Create new file** 모달 창의 **Name** 필드에 `.dockerignore`를 입력하고 **Create file** 버튼을 클릭합니다.
* 아래 내용을 복사하여 붙여넣습니다.

    ```text
    node_modules
    npm-debug.log
    ```

* **Commit...** 버튼을 클릭한 다음, `Commit to main branch`를 선택하고 **Commit** 버튼을 클릭합니다.

### Kubernetes manifest 템플릿 파일 생성

다음을 수행하여 애플리케이션 배포에 필요한 Kubernetes 리소스를 생성하기 위한 쿠버네티스 매니페스트 템플릿 파일을 생성합니다.

* **Web IDE** 페이지의 왼쪽 사이드 바에서 폴더 모양의 **New directory** 아이콘을 클릭합니다.
* **Create new directory** 모달 창의 **Name** 필드에 `.k8s`를 입력하고 **Create directory** 버튼을 클릭합니다.
* **.k8s** 디렉토리를 선택한 후, 오른쪽의 **⋮** 아이콘을 선택하고 **New file**을 선택합니다.
* **Create new file** 모달 창의 **Name** 필드에 `.k8s/deploy.template.yaml`를 입력하고 **Create file** 버튼을 클릭합니다.
* 아래 내용을 복사하여 붙여넣습니다.

    ```yaml
    #----------------------------
    # Kubernetes manifests
    #----------------------------
    ---
    # Deployment
    apiVersion: apps/v1
    kind: Deployment
    metadata:
      name: ${APPLICATION_NAME}-deployment
      annotations:
        app.gitlab.com/app: ${CI_PROJECT_PATH_SLUG}
        app.gitlab.com/env: ${CI_ENVIRONMENT_SLUG}
    spec:
      replicas: ${REPLICAS}
      selector:
        matchLabels:
          app: ${APPLICATION_NAME}
      template:
        metadata:
          labels:
            app: ${APPLICATION_NAME}
          annotations:
            app.gitlab.com/app: ${CI_PROJECT_PATH_SLUG}
            app.gitlab.com/env: ${CI_ENVIRONMENT_SLUG}
        spec:
          containers:
            - name: ${APPLICATION_NAME}
              image: ${IMAGE}
              ports:
                - containerPort: ${CONTAINER_PORT}
                  protocol: TCP
              imagePullPolicy: Always
          imagePullSecrets:
            - name: image-pull-secret
    ---
    # Service
    apiVersion: v1
    kind: Service
    metadata:
      name: ${APPLICATION_NAME}-service
      labels:
        app: ${APPLICATION_NAME}
    spec:
      type: NodePort
      selector:
        app: ${APPLICATION_NAME}
      ports:
        - protocol: TCP
          port: ${EXPOSED_PORT}
          targetPort: ${CONTAINER_PORT}
    ---
    # Ingress
    apiVersion: networking.k8s.io/v1
    kind: Ingress
    metadata:
      name: ${APPLICATION_NAME}-ingress
      annotations:
        kubernetes.io/ingress.class: "gitlab-ingress-nginx"
    spec:
      rules:
        - host: ${HOST}
          http:
            paths:
              - pathType: ImplementationSpecific
                path: /
                backend:
                  service:
                    name: ${APPLICATION_NAME}-service
                    port:
                      number: ${EXPOSED_PORT}
    ```

* **Commit...** 버튼을 클릭한 다음, `Commit to main branch`를 선택하고 **Commit** 버튼을 클릭합니다.

### `.gitlab-ci.yml` 파일 생성

* **Web IDE** 페이지의 왼쪽 사이드 바에서 **New file** 아이콘을 클릭합니다.
* **Create new file** 모달 창의 **Name** 필드에 `.gitlab-ci.yml`를 입력하고 **Create file** 버튼을 클릭합니다.
* 아래 내용을 복사하여 붙여넣습니다.

    ```yaml
    stages:
      - build
      - deploy

    #----------------------------
    # Build Docker image
    #----------------------------
    🐳docker-build:
      image: docker:latest
      stage: build
      variables:
        DOCKER_DRIVER: overlay2
        DOCKER_TLS_CERTDIR: ""
      services:
        - docker:dind
      before_script:
        - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
      script:
        - docker pull $CI_REGISTRY_IMAGE:latest || true
        - docker build --cache-from $CI_REGISTRY_IMAGE:latest --tag $CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA --tag $CI_REGISTRY_IMAGE:latest .
        - docker push $CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA
        - docker push $CI_REGISTRY_IMAGE:latest

    #----------------------------
    # YAML Anchors
    #----------------------------
    .environment_variables: &environment_variables
    - |
      export CONTAINER_PORT=${CONTAINER_PORT:-3000}
      export EXPOSED_PORT=${EXPOSED_PORT:-80}
      export APPLICATION_NAME=${CI_PROJECT_NAME}
      export REPLICAS=1
      export NAMESPACE=hello-ns
      export TAG=${CI_COMMIT_SHORT_SHA}
      export IMAGE=$CI_REGISTRY_IMAGE:$TAG
      export BASE_DOMAIN="${KUBE_INGRESS_BASE_DOMAIN}"
      export BRANCH=${CI_COMMIT_REF_SLUG}
      export HOST="${APPLICATION_NAME}.${BRANCH}.${BASE_DOMAIN}"

    .environment_variables_substitution: &environment_variables_substitution
    - |
      envsubst < ./.k8s/deploy.template.yaml > ./.k8s/deploy.${TAG}.yaml
      echo "Substitution -> ./.k8s/deploy.${TAG}.yaml"
      cat ./.k8s/deploy.${TAG}.yaml

    .ensure_secret: &ensure_secret
    - |
      echo "Checking secret.."
      kubectl describe secrets image-pull-secret -n "$NAMESPACE" || \
        kubectl create secret -n "$NAMESPACE" \
          docker-registry image-pull-secret \
          --docker-server="$CI_REGISTRY" \
          --docker-username="${DOCKER_USERNAME:-${CI_REGISTRY_USER}}" \
          --docker-password="${DOCKER_PASSWORD:-${CI_REGISTRY_PASSWORD}}" \
          --docker-email="${DOCKER_EMAIL:-${GITLAB_USER_EMAIL}}" \
          -o yaml --dry-run | kubectl replace -n "$NAMESPACE" --force -f -

    .kubectl_apply: &kubectl_apply
    - |
      kubectl apply -f ./.k8s/deploy.${TAG}.yaml -n ${NAMESPACE}

    .display_information: &display_information
    - |
      echo "http://${HOST}"
      kubectl get pods --namespace ${NAMESPACE}

    #----------------------------
    # Deploy
    #----------------------------
    🚀deploy-to-production:
      stage: deploy
      image: k33g/k3g.utilities:1.0.0
      only:
        - main
      environment:
        name: production/${CI_PROJECT_NAME}-${CI_COMMIT_REF_SLUG}
        url: http://${HOST}
      script:
        - *environment_variables
        - *environment_variables_substitution
        - *ensure_secret
        - *kubectl_apply
        - *display_information
    ```

* **Commit...** 버튼을 클릭한 다음, `Commit to main branch`를 선택하고 **Commit** 버튼을 클릭합니다.

### CD 파이프라인 결과 확인

* 사이드 바에서 **CI/CD > Pipelines**을 클릭하면 CD 파이프라인이 실행되는 것을 확인할 수 있습니다.
* 파이프라인 일련번호를 클릭하면 파이프라인 그래프를 볼 수 있습니다.

    ![GitLab Pipeline graph](images/09/gitlab_pipeline_graph.png "GitLab Pipeline graph")

* 각 Job을 클릭하면 로그를 확인할 수 있습니다.

### 배포 결과 확인

아래 명령을 실행하여 Ingress의 Host 정보를 확인합니다.

```bash
$ kubectl get ingress -n hello-ns
NAME                    CLASS    HOSTS                                   ADDRESS         PORTS   AGE
deploy-to-gke-ingress   <none>   deploy-to-gke.main.172.64.5.11.nip.io   172.64.5.11     80      35s
```

아래 명령을 실행하여 배포된 애플리케이션으로 접속되는 것을 확인할 수 있습니다.

```bash
$ curl http://deploy-to-gke.main.172.64.5.11.nip.io
Hello GitLab CI/CD!%
```

또는 웹 브라우저에서 `http://deploy-to-gke.main.172.64.5.11.nip.io`로 접속합니다.
