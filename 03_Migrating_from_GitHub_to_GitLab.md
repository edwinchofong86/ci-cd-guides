# GitHub에서 GitLab으로 마이그레이션 가이드

본 가이드에서는 GitHub 리포지토리를 GitLab.com의 프로젝트로 마이그레이션하는 방법을 설명합니다.

GitLab의 가져오기 도구를 사용하여 GitHub 리포지토리의 다음 항목을 가져올 수 있습니다.

* 리포지토리 설명 (GitLab.com 및 7.7+)
* Git repository 데이터 (GitLab.com 및 7.7+)
* Issues (GitLab.com 및 7.7+)
* Pull requests (GitLab.com 및 8.4 이상)
* Wiki 페이지 (GitLab.com 및 8.4 이상)
* 마일스톤 (GitLab.com 및 8.7 이상)
* 레이블 (GitLab.com 및 8.7 이상)
* 릴리즈 노트 설명 (GitLab.com 및 8.12+)
* Pull request 리뷰 코멘트 (GitLab.com & 10.2+)
* Pull request 리뷰 (GitLab.com & 13.7+)
* Pull request “merged by” 정보 (GitLab.com & 13.7+)
* 정기적인 이슈 및 Pull request 코멘트
* [Git LFS (대용량 파일 저장소) 개체](https://docs.gitlab.com/ee/topics/git/lfs/index.html)

## 작동 원리

이슈(Issues)와 풀 리퀘스트(Pull requests)를 가져올 때 가져오기 도구는 GitLab 인스턴스의 데이터베이스에서 GitHub 작성자 및 담당자를 찾으려고 시도합니다. (GitLab에서는 풀 리퀘스트를 "Merge requests"라고 함)

이 연결이 성공하려면 저장소의 각 GitHub 작성자 및 담당자가 가져오기 전에 다음 조건 중 하나를 충족해야 합니다.

* 이전에 GitHub 아이콘을 사용하여 GitLab 계정에 로그인
* GitLab 계정의 이메일 주소와 일치하는 [공개 이메일 주소](https://docs.github.com/en/github/setting-up-and-managing-your-github-user-account/setting-your-commit-email-address)가 있는 GitHub 계정 소유

> GitHub 계정을 사용하는 GitLab 콘텐츠를 가져오려면 GitHub 공개 이메일 주소가 채워져야 합니다. 그러면 모든 코멘트와 기여가 GitLab의 동일한 사용자에게 올바르게 매핑됩니다. GitHub Enterprise(온 프레미스)에서는 제품을 사용하기 위해 이 필드를 채울 필요가 없으므로 가져온 콘텐츠가 새 시스템의 사용자에게 제대로 매핑되도록 기존 GitHub Enterprise 계정에 추가해야 할 수 있습니다. 해당 주소를 추가하는 방법에 대한 지침은 GitHub 설명서를 참조하십시오.

프로젝트에서 참조된 사용자가 GitLab 데이터베이스에 없는 경우, 프로젝트 작성자 (일반적으로 가져오기 프로세스를 시작한 사용자)가 작성자/담당자로 설정되지만, 원래 GitHub 작성자를 언급하는 이슈에 대한 메모가 추가됩니다.

가져오기 도구는 새 네임스페이스(그룹)가 존재하지 않는 경우 생성하거나, 네임스페이스를 사용하는 경우 가져오기 프로세스를 시작한 사용자의 네임스페이스 아래에 리포지토리를 가져옵니다. 적절한 권한으로 네임스페이스/리포지토리 이름을 편집할 수도 있습니다.

가져오기 도구는 오픈 풀 리퀘스트와 관련된 프로젝트 포크(Forks)의 브랜치도 가져옵니다. 이러한 브랜치는 `GH-SHA-username/pull-request-number/fork-name/branch`와 유사한 명명 체계를 사용하여 가져옵니다. 이로 인해 GitHub 리포지토리와 비교하여 브랜치에서 불일치가 발생할 수 있습니다.

추가 기술 세부 정보는 [GitHub Importer](https://docs.gitlab.com/ee/development/github_importer.html) 개발자 문서를 참조하세요.

가져오기 프로세스에 대한 개요는 [GitHub에서 GitLab으로 마이그레이션](https://youtu.be/VYOXuOg9tQI) 비디오를 참조하세요.

## GitHub 리포지토리를 GitLab으로 가져오기

시작하기 전에 GitLab 사용자에게 매핑하려는 GitHub 사용자가 다음 중 하나를 가지고 있는지 확인합니다.

* GitHub 아이콘을 사용하여 로그인 한 GitLab 계정
* GitHub 사용자 프로필에서 [공개적으로 표시되는 이메일 주소](https://docs.github.com/en/rest/reference/users#get-a-user)와 일치하는 이메일 주소가 있는 GitLab 계정

사용자 일치 시도는 해당 순서로 진행되며, 사용자를 어떤 방식으로도 식별할 수 없으면, 작업은 가져오기를 수행하는 사용자 계정과 연관됩니다.

> 자체 관리형 GitLab 인스턴스를 사용 중이거나 GitHub Enterprise에서 가져오는 경우, 이 프로세스를 수행하려면 [GitHub 통합](https://docs.gitlab.com/ee/integration/github.html)을 구성해야 합니다.

* 상단 내비게이션 바에서 **[+]** 아이콘을 클릭하고 **New project**를 선택하거나, 그룹에서 **New project** 버튼을 클릭합니다.
* **Create new project** 페이지에서 **Import project**를 클릭합니다.
* **Import project** 페이지에서 **GitHub** 버튼을 클릭합니다.
  ![GitLab Import project](images/03/gitlab_import_project.png "GitLab Import project")
* **Authenticate with GitHub** 페이지에서 **Authenticate with GitHub** 버튼을 클릭합니다.
  ![Authenticate with GitHub](images/03/gitlab_authenticate_with_github.png "Authenticate with GitHub")

  > GitLab.com으로 가져오는 경우, [개인 액세스 토큰](https://docs.gitlab.com/ee/user/project/import/github.html#use-a-github-token)을 사용하여 GitHub 리포지토리를 가져올 수도 있습니다. 이 방법은 모든 사용자 활동(예 : 이슈 및 풀 리퀘스트)을 일치하는 GitLab 사용자와 연결하지 않으므로 권장하지 않습니다.

* **GitHub**의 **Sign in** 페이지로 리다이렉션 됩니다.  
  ![Sign in to GitHub](images/03/gitlab_sign_in_to_github.png "Sign in to GitHub")
* GitHub에 로그인하면 **Authorize GitLab.com** 페이지로 이동됩니다.  
  ![Authorize GitLab.com](images/03/gitlab_authorize_gitlab.com.png "Authorize GitLab.com")
* **Authorize GitLab.com** 페이지의 **Organization access** 섹션에서 원하는 조직의 **Grant** 버튼을 클릭합니다.
* **Authorize gitlabhq** 버튼을 클릭합니다.

GitHub 리포지토리에 대한 액세스를 승인하면 GitHub에서 가져오기 페이지로 리디렉션되고 GitHub 리포지토리가 나열됩니다.

* 기본적으로 제안된 리포지토리 네임스페이스는 GitHub에 존재하는 이름과 일치하지만, 권한에 따라 이러한 이름을 가져오기 전에 편집하도록 선택할 수 있습니다.
* 원하는 수의 리포지토리 옆에 있는 **Import** 버튼을 클릭하거나, 모든 리포지토리를 가져오려면 **Import OO repositories** 버튼을 클릭합니다. 또한 이름으로 프로젝트를 필터링할 수 있습니다. 필터가 적용된 경우, 모든 리포지토리 가져오기는 일치하는 리포지토리만 가져옵니다.
* **Status** 컬럼은 각 리포지토리의 가져오기 상태를 보여줍니다. 페이지를 열어 두도록 선택할 수 있으며 실시간으로 업데이트되거나 나중에 다시 돌아갈 수 있습니다.
* **Import repositories from GitHub** 페이지에서 가져올 리포지토리의 GitLab 네임스페이스를 검색하여 선택한 다음 **Import** 버튼을 클릭합니다.
  ![Import repositories from GitHub](images/03/gitlab_import_repositories_from_github.png "Import repositories from GitHub")
* 잠시 후 Importing이 완료되어 Status가 **Complete**로 변경되면 **Go to project** 버튼을 클릭하여 마이그레이션 된 GitLab 프로젝트로 이동합니다.

## GitLab으로 마이그레이션 결과

아래 이미지와 같이 **GitHub**의 리포지토리 설명, Git repository 데이터(커밋 이력 및 파일)를 **GitLab** 프로젝트의 Repository로 가져온 것을 확인할 수 있습니다.

  ![GitHub Repository Code](images/03/github_repository_code.png "GitHub Repository Code")
  ![Imported GitLab project](images/03/gitlab_project_details.png "Imported GitLab project")

아래와 같이 마일스톤을 가져옵니다. GitHub에서는 풀 리퀘스트(Pull requests)도 포함되어 진도율은 차이가 있습니다.

  ![GitHub Milestone](images/03/github_milestone.png "GitHub Milestone")
  ![GitLab Milestone](images/03/gitlab_milestone.png "GitLab Milestone")

GitHub 리포지토리가 생성될 때 기본으로 포함되는 기본 레이블(Default labels)뿐만 아니라 `custom label`과 같이 사용자가 추가로 생성한 사용자 정의 레이블도 가져옵니다.

  ![GitHub Labels](images/03/github_labels.png "GitHub Labels")
  ![GitLab Labels](images/03/gitlab_labels.png "GitLab Labels")

마일스톤 및 레이블 등 참조 정보를 포함한 이슈를 가져옵니다.

  ![GitHub Issues](images/03/github_issues.png "GitHub Issues")
  ![GitLab Issues](images/03/gitlab_issues.png "GitLab Issues")

GitHub의 풀 리퀘스트(Pull requests)를 GitLab 머지 리퀘스트(Merge requests)로 가져옵니다.

  ![GitHub Pull requests](images/03/github_pull_requests.png "GitHub Pull requests")
  ![GitLab Merge requests](images/03/gitlab_merge_requests.png "GitLab Merge requests")
